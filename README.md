# 맛딱드림 프로젝트
##### Matddak Drim Project

2024.03.05 ~ 2024.05.07

![logo](./images/matddak_logo.png)

기존 라이더 어플의 UI/UX를 개선하여 좀 더 한 눈에 보이고 쉽게 작동할 수 있도록 제작하였습니다.
***
### 😄 개발자 소개

- **GmJu** (PM) : 백엔드 [[ 😺GitLap ]](https://gitlab.com/myungdon)
- **McKang** (PL) : 앱 프론트엔드 [[ 😺GitLap ]](https://gitlab.com/kmjkali)
- **IuPark** : 웹 프론트엔드 [[ 😺GitLap ]](https://gitlab.com/meimeipark)
- **WbBae** : 웹 프론트엔드[[ 😺GitLap ]](https://gitlab.com/babasw135)
- **RgKim** : 백엔드 [[ 😺GitLap ]](https://gitlab.com/namho0172)
- **FxNull** : 앱 프론트엔드 [[ 😺GitLap ]](https://gitlab.com/hamsubin0628)
***
### 📱 프로젝트 주요 기능

1. jwt 토큰을 이용한 로그인, 
2. 배차부터 완료까지 클릭 한번으로 상태 값 변화, 
3. 수익 내역 보기
***
### 🛠️ 기술 스택

### * Java
### * Spring Boot
### * REST API
### * JWT Token Security
### * Swagger
### * Docker
### * PostgreSQL
### * Google Cloud Platform

## 6. 프로젝트 아키텍처
![architecture](./images/architecture.png)
***
## 7. 릴리즈 노트
* [릴리즈 노트 구글 시트](https://docs.google.com/spreadsheets/d/1lGoJcs1v2O8cRTf9it5CY55024iYrIXxEypsIwqz5Jk/edit#gid=0) 바로 가기
