package com.mj.matddakapi.model.store;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

@Getter
@Setter
public class StoreChangeRequest {
    @NotNull
    @Length(min = 1, max = 30)
    @Schema(description = "가게 이름")
    private String storeName;

    @NotNull
    @Length(min = 10, max = 100)
    @Schema(description = "가게 주소 도로명")
    private String addressStoreDo;

    @Length(min = 10, max = 100)
    @Schema(description = "가게 주소 지번")
    private String addressStoreG;

    @NotNull
    @Schema(description = "가게 좌표 위도")
    private Double storeX;

    @NotNull
    @Schema(description = "가게 좌표 경도")
    private Double storeY;

    @NotNull
    @Length(min = 13, max = 20)
    @Schema(description = "가게 전화번호")
    private String storeNumber;

    @NotNull
    @Length(min = 1, max = 50)
    @Schema(description = "운영 시간")
    private String sellTime;

    @NotNull
    @Length(min = 1, max = 30)
    @Schema(description = "휴무일")
    private String holiday;

    @NotNull
    @Length(min = 1, max = 50)
    @Schema(description = "배달 지역")
    private String deliveryArea;

    @NotNull
    @Length(min = 1, max = 20)
    @Schema(description = "대표자명")
    private String bossName;

    @NotNull
    @Length(min = 1, max = 30)
    @Schema(description = "사업자 번호")
    private String shopId;
}
