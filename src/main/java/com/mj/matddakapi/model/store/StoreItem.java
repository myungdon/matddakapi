package com.mj.matddakapi.model.store;

import com.mj.matddakapi.entity.Store;
import com.mj.matddakapi.interfaces.CommonModelBuilder;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StoreItem {
    @Schema(description = "시퀀스")
    private Long id;

    @Schema(description = "가게 이름")
    private String storeName;

    @Schema(description = "가게 주소 도로명")
    private String addressStoreDo;

    @Schema(description = "가게 주소 지번")
    private String addressStoreG;

    @Schema(description = "가게 좌표 위도")
    private Double storeX;

    @Schema(description = "가게 좌표 경도")
    private Double storeY;

    @Schema(description = "가게 전화번호")
    private String storeNumber;

    @Schema(description = "운영 시간")
    private String sellTime;

    @Schema(description = "휴무일")
    private String holiday;

    @Schema(description = "배달 지역")
    private String deliveryArea;

    @Schema(description = "대표자명")
    private String bossName;

    @Schema(description = "사업자 번호")
    private String shopId;

    private StoreItem(Builder builder) {
        this.id = builder.id;
        this.storeName = builder.storeName;
        this.addressStoreDo = builder.addressStoreDo;
        this.addressStoreG = builder.addressStoreG;
        this.storeX = builder.storeX;
        this.storeY = builder.storeY;
        this.storeNumber = builder.storeNumber;
        this.sellTime = builder.sellTime;
        this.holiday = builder.holiday;
        this.deliveryArea = builder.deliveryArea;
        this.bossName = builder.bossName;
        this.shopId = builder.shopId;
    }

    public static class Builder implements CommonModelBuilder<StoreItem> {
        private final Long id;
        private final String storeName;
        private final String addressStoreDo;
        private final String addressStoreG;
        private final Double storeX;
        private final Double storeY;
        private final String storeNumber;
        private final String sellTime;
        private final String holiday;
        private final String deliveryArea;
        private final String bossName;
        private final String shopId;

        public Builder(Store store) {
            this.id = store.getId();
            this.storeName = store.getStoreName();
            this.addressStoreDo = store.getAddressStoreDo();
            this.addressStoreG = store.getAddressStoreG();
            this.storeX = store.getStoreX();
            this.storeY = store.getStoreY();
            this.storeNumber = store.getStoreNumber();
            this.sellTime = store.getSellTime();
            this.holiday = store.getHoliday();
            this.deliveryArea = store.getDeliveryArea();
            this.bossName = store.getBossName();
            this.shopId = store.getShopId();
        }

        @Override
        public StoreItem build() {
            return new StoreItem(this);
        }
    }
}
