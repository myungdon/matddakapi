package com.mj.matddakapi.model.income;

import com.mj.matddakapi.entity.Income;
import com.mj.matddakapi.interfaces.CommonModelBuilder;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class IncomeResponse {
    @Schema(description = "시퀀스")
    private Long id;

    @Schema(description = "배송 ID")
    private Long delivery;

    @Schema(description = "라이더 수익")
    private Double feeRider;

    @Schema(description = "관리자 수익")
    private Double feeAdmin;

    @Schema(description = "배달비")
    private Double feeTotal;

    @Schema(description = "산재 보험료")
    private Double taxSan;

    @Schema(description = "고용 보험료")
    private Double taxGo;

    @Schema(description = "수익 발생일")
    private LocalDate dateIncome;

    @Schema(description = "수익 발생 시간")
    private LocalDateTime timeIncome;

    public IncomeResponse(Builder builder) {
        this.id = builder.id;
        this.delivery = builder.delivery;
        this.feeRider = builder.feeRider;
        this.feeAdmin = builder.feeAdmin;
        this.feeTotal = builder.feeTotal;
        this.taxSan = builder.taxSan;
        this.taxGo = builder.taxGo;
        this.dateIncome = builder.dateIncome;
        this.timeIncome = builder.timeIncome;
    }

    public static class Builder implements CommonModelBuilder<IncomeResponse> {
        private final Long id;

        private final Long delivery;

        private final Double feeRider;

        private final Double feeAdmin;

        private final Double feeTotal;

        private final Double taxSan;

        private final Double taxGo;

        private final LocalDate dateIncome;

        private final LocalDateTime timeIncome;

        public Builder(Income income) {
            this.id = income.getId();
            this.delivery = income.getDelivery().getId();
            this.feeRider = income.getFeeRider();
            this.feeAdmin = income.getFeeAdmin();
            this.feeTotal = income.getFeeTotal();
            this.taxSan = income.getTaxSan();
            this.taxGo = income.getTaxGo();
            this.dateIncome = income.getDateIncome();
            this.timeIncome = income.getTimeIncome();
        }

        @Override
        public IncomeResponse build() {
            return new IncomeResponse(this);
        }
    }
}
