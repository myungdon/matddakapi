package com.mj.matddakapi.model.riderpay;

import com.mj.matddakapi.entity.RiderPay;
import com.mj.matddakapi.interfaces.CommonModelBuilder;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class RiderPayItem {
    @Schema(description = "시퀀스")
    private Long id;

    @Schema(description = "라이더 ID")
    private Long riderId;

    @Schema(description = "라이더 이름")
    private String riderName;

    @Schema(description = "현재 페이")
    private Double payNow;

    public RiderPayItem(Builder builder) {
        this.id = builder.id;
        this.riderId = builder.riderId;
        this.riderName = builder.riderName;
        this.payNow = builder.payNow;
    }

    public static class Builder implements CommonModelBuilder<RiderPayItem> {
        private final Long id;
        private final Long riderId;
        private final String riderName;
        private final Double payNow;

        public Builder(RiderPay riderPay) {
            this.id = riderPay.getId();
            this.riderId = riderPay.getRider().getId();
            this.riderName = riderPay.getRider().getName();
            this.payNow = riderPay.getPayNow();
        }
        @Override
        public RiderPayItem build() {
            return new RiderPayItem(this);
        }
    }
}
