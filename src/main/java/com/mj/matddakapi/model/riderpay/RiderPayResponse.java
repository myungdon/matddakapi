package com.mj.matddakapi.model.riderpay;

import com.mj.matddakapi.entity.RiderPay;
import com.mj.matddakapi.interfaces.CommonModelBuilder;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class RiderPayResponse {
    @Schema(description = "라이더 이름")
    private String riderName;

    @Schema(description = "현재 페이")
    private Double payNow;

    @Schema(description = "은행명")
    private String bankName;

    @Schema(description = "계좌 번호")
    private String bankNumber;

    @Schema(description = "예금주명")
    private String bankOwner;

    public RiderPayResponse(Builder builder) {
        this.riderName = builder.riderName;
        this.payNow = builder.payNow;
        this.bankName = builder.bankName;
        this.bankNumber = builder.bankNumber;
        this.bankOwner = builder.bankOwner;
    }

    public static class Builder implements CommonModelBuilder<RiderPayResponse> {
        private final String riderName;
        private final Double payNow;
        private final String bankName;
        private final String bankNumber;
        private final String bankOwner;

        public Builder(RiderPay riderPay) {
            this.riderName = riderPay.getRider().getName();
            this.payNow = riderPay.getPayNow();
            this.bankName = riderPay.getRider().getBankName().getName();
            this.bankNumber = riderPay.getRider().getBankNumber();
            this.bankOwner = riderPay.getRider().getBankOwner();
        }
        @Override
        public RiderPayResponse build() {
            return new RiderPayResponse(this);
        }
    }
}
