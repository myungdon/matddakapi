package com.mj.matddakapi.model.dash;

import com.mj.matddakapi.entity.Board;
import com.mj.matddakapi.entity.Rider;
import com.mj.matddakapi.interfaces.CommonModelBuilder;
import com.mj.matddakapi.model.board.BoardItem;
import com.mj.matddakapi.model.rider.RiderItem;
import com.mj.matddakapi.model.statistics.NearDayStatisticsResponse;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class DashResponse {
    private NearDayStatisticsResponse nearDayStatistics;;
    private List<RiderItem> riderItems;
    private List<BoardItem> boardItems;

    public DashResponse(Builder builder) {
        this.nearDayStatistics = builder.nearDayStatistics;
        this.riderItems = builder.riderItems;
        this.boardItems = builder.boardItems;
    }

    public static class Builder implements CommonModelBuilder<DashResponse> {
        private final NearDayStatisticsResponse nearDayStatistics;;
        private List<RiderItem> riderItems;
        private List<BoardItem> boardItems;

        public Builder(NearDayStatisticsResponse nearDayStatistics, List<RiderItem> riderItems, List<BoardItem> boardItems) {
            this.nearDayStatistics = nearDayStatistics;
            this.riderItems = riderItems;
            this.boardItems = boardItems;
        }

        public Builder riderItems(Rider rider) {
            this.riderItems = rider == null ? null : riderItems;
            return this;
        }

        public Builder boardItems(Board board) {
            this.boardItems = board == null ? null : boardItems;
            return this;
        }

        @Override
        public DashResponse build() {
            return new DashResponse(this);
        }
    }
}
