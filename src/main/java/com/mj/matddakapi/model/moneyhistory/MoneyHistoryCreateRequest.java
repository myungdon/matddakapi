package com.mj.matddakapi.model.moneyhistory;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MoneyHistoryCreateRequest {
    @NotNull
    @Schema(description = "금액")
    private Double moneyAmount;

    @Schema(description = "메모")
    private String etcMemo;
}
