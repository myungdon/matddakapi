package com.mj.matddakapi.model.moneyhistory;

import com.mj.matddakapi.entity.MoneyHistory;
import com.mj.matddakapi.interfaces.CommonModelBuilder;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.time.format.DateTimeFormatter;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MoneyHistoryResponse {
    @Schema(description = "시퀀스")
    private Long id;

    @Schema(description = "라이더 ID")
    private Long riderId;

    @Schema(description = "라이더 이름")
    private String riderName;

    @Schema(description = "입출금 타입")
    private String moneyType;

    @Schema(description = "금액")
    private Double moneyAmount;

    @Schema(description = "입출금일")
    private String dateMoneyRenewal;

    @Schema(description = "메모")
    private String etcMemo;

    public MoneyHistoryResponse(Builder builder) {
        this.id = builder.id;
        this.riderId = builder.riderId;
        this.riderName = builder.riderName;
        this.moneyType = builder.moneyType;
        this.moneyAmount = builder.moneyAmount;
        this.dateMoneyRenewal = builder.dateMoneyRenewal;
        this.etcMemo = builder.etcMemo;
    }

    public static class Builder implements CommonModelBuilder<MoneyHistoryResponse> {
        private final Long id;
        private final Long riderId;
        private final String riderName;
        private final String moneyType;
        private final Double moneyAmount;
        private final String dateMoneyRenewal;
        private final String etcMemo;

        public Builder(MoneyHistory moneyHistory) {
            this.id = moneyHistory.getId();
            this.riderId = moneyHistory.getRider().getId();
            this.riderName = moneyHistory.getRider().getName();
            this.moneyType = moneyHistory.getMoneyType().getName();
            this.moneyAmount = moneyHistory.getMoneyAmount();
            this.dateMoneyRenewal = moneyHistory.getDateMoneyRenewal().format(DateTimeFormatter.ofPattern("yyyy년 MM월 dd일 HH:mm:ss"));
            this.etcMemo = moneyHistory.getEtcMemo();
        }

        @Override
        public MoneyHistoryResponse build() {
            return new MoneyHistoryResponse(this);
        }
    }
}
