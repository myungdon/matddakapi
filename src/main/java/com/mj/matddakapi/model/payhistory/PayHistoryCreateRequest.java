package com.mj.matddakapi.model.payhistory;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PayHistoryCreateRequest {
    @NotNull
    @Schema(description = "페이 액수")
    private Double payAmount;
}
