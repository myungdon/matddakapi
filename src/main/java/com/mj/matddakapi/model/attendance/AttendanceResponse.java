package com.mj.matddakapi.model.attendance;

import com.mj.matddakapi.entity.Attendance;
import com.mj.matddakapi.interfaces.CommonModelBuilder;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class AttendanceResponse {
    @Schema(description = "라이더 이름")
    private String riderName;

    @Schema(description = "운행 상태")
    private String isWork;

    @Schema(description = "근태 시간 체크")
    private String dateCheckWork;

    public AttendanceResponse(Builder builder) {
        this.riderName = builder.riderName;
        this.isWork = builder.isWork;
        this.dateCheckWork = builder.dateCheckWork;
    }

    public static class Builder implements CommonModelBuilder<AttendanceResponse> {
        private final String riderName;
        private final String isWork;
        private final String dateCheckWork;

        public Builder(Attendance attendance) {
            this.riderName = attendance.getRider().getName();
            this.isWork = attendance.getIsWork() ? "정상업무" : "휴식";
            this.dateCheckWork = attendance.getDateCheckWork();
        }
        @Override
        public AttendanceResponse build() {
            return new AttendanceResponse(this);
        }
    }
}
