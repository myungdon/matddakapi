package com.mj.matddakapi.model.ask;

import com.mj.matddakapi.entity.Ask;
import com.mj.matddakapi.interfaces.CommonModelBuilder;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class AskItem {
    @Schema(description = "시퀀스")
    private Long id;

    @Schema(description = "가게 ID")
    private Long storeId;

    @Schema(description = "가게 이름")
    private String storeName;

    @Schema(description = "요청 상태")
    private String stateAsk;

    @Schema(description = "주문일")
    private String dateAsk;

    @Schema(description = "주문시간")
    private String timeAsk;

    @Schema(description = "음식 메뉴")
    private String askMenu;

    @Schema(description = "음식 가격")
    private Double priceFood;

    @Schema(description = "배달료(고객과 가게가 내는)")
    private Double priceRide;

    @Schema(description = "결제 금액")
    private Double priceTotal;

    @Schema(description = "배달비(라이더+관리자+세금 수익)")
    private Double feeTotal;

    @Schema(description = "선결제 여부")
    private String isPay;

    @Schema(description = "결제 수단")
    private String howPay;

    @Schema(description = "가게 요청사항")
    private String requestStore;

    @Schema(description = "라이더 요청사항")
    private String requestRider;

    @Schema(description = "고객 번호")
    private String phoneClient;

    @Schema(description = "가게 주소(도로명)")
    private String addressStoreDo;

    @Schema(description = "고객 주소(도로명)")
    private String addressClientDo;

    @Schema(description = "고객 주소(지번)")
    private String addressClientG;

    @Schema(description = "고객 좌표 위도")
    private Double clientX;

    @Schema(description = "고객 좌표 경도")
    private Double clientY;

    @Schema(description = "배송거리")
    private Double distance;

    public AskItem(Builder builder) {
        this.id = builder.id;
        this.storeId = builder.storeId;
        this.storeName = builder.storeName;
        this.stateAsk = builder.stateAsk;
        this.dateAsk = builder.dateAsk;
        this.timeAsk = builder.timeAsk;
        this.askMenu = builder.askMenu;
        this.priceFood = builder.priceFood;
        this.priceRide = builder.priceRide;
        this.priceTotal = builder.priceTotal;
        this.feeTotal = builder.feeTotal;
        this.isPay = builder.isPay;
        this.howPay = builder.howPay;
        this.requestStore = builder.requestStore;
        this.requestRider = builder.requestRider;
        this.phoneClient = builder.phoneClient;
        this.addressStoreDo = builder.addressStoreDo;
        this.addressClientDo = builder.addressClientDo;
        this.addressClientG = builder.addressClientG;
        this.clientX = builder.clientX;
        this.clientY = builder.clientY;
        this.distance = builder.distance;
    }

    public static class Builder implements CommonModelBuilder<AskItem> {
        private final Long id;
        private final Long storeId;
        private final String storeName;
        private final String stateAsk;
        private final String dateAsk;
        private final String timeAsk;
        private final String askMenu;
        private final Double priceFood;
        private final Double priceRide;
        private final Double priceTotal;
        private final Double feeTotal;
        private final String isPay;
        private final String howPay;
        private final String requestStore;
        private final String requestRider;
        private final String phoneClient;
        private final String addressStoreDo;
        private final String addressClientDo;
        private final String addressClientG;
        private final Double clientX;
        private final Double clientY;
        private final Double distance;

        public Builder(Ask ask) {
            this.id = ask.getId();
            this.storeId = ask.getStore().getId();
            this.storeName = ask.getStore().getStoreName();
            this.stateAsk = ask.getStateAsk().getName();
            this.dateAsk = ask.getDateAsk().format(DateTimeFormatter.ofPattern("yyyy년 MM월 dd일"));
            this.timeAsk = ask.getTimeAsk();
            this.askMenu = ask.getAskMenu();
            this.priceFood = ask.getPriceFood();
            this.priceRide = ask.getPriceRide();
            this.priceTotal = ask.getPriceTotal();
            this.feeTotal = (double) Math.round(priceRide * 0.6);
            this.isPay = ask.getIsPay() ? "선" : "후";
            this.howPay = ask.getHowPay().getName();
            this.requestStore = ask.getRequestStore();
            this.requestRider = ask.getRequestRider();
            this.phoneClient = ask.getPhoneClient();
            this.addressStoreDo = ask.getStore().getAddressStoreDo();
            this.addressClientDo = ask.getAddressClientDo();
            this.addressClientG = ask.getAddressClientG();
            this.clientX = ask.getClientX();
            this.clientY = ask.getClientY();
            this.distance = ask.getDistance();
        }

        @Override
        public AskItem build() {
            return new AskItem(this);
        }
    }
}
