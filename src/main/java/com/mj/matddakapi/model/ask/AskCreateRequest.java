package com.mj.matddakapi.model.ask;

import com.mj.matddakapi.enums.ask.HowPay;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

@Getter
@Setter
public class AskCreateRequest {
    @NotNull
    @Length(min = 3, max = 50)
    @Schema(description = "음식 메뉴", minLength = 3, maxLength = 50)
    private String askMenu;

    @NotNull
    @Schema(description = "음식 가격")
    private Double priceFood;

    @NotNull
    @Schema(description = "선결제 여부")
    private Boolean isPay;

    @NotNull
    @Schema(description = "결제 수단")
    private HowPay howPay;

    @Schema(description = "가게 요청사항")
    private String requestStore;

    @Schema(description = "라이더 요청사항")
    private String requestRider;

    @NotNull
    @Length(min = 10, max = 100)
    @Schema(description = "고객 번호", minLength = 10, maxLength = 100)
    private String phoneClient;

    @NotNull
    @Length(min = 10, max = 100)
    @Schema(description = "고객 주소 도로명", minLength = 10, maxLength = 100)
    private String addressClientDo;

    @Schema(description = "고객 주소 지번")
    private String addressClientG;

    @NotNull
    @Schema(description = "고객 좌표 위도")
    private Double clientX;

    @NotNull
    @Schema(description = "고객 좌표 경도")
    private Double clientY;
}
