package com.mj.matddakapi.model.rider;

import com.mj.matddakapi.enums.rider.*;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import java.time.LocalDateTime;

@Getter
@Setter
public class RiderChangeRequest {
    @NotNull
    @Schema(description = "회원 등급")
    private Admin Admin;

    @NotNull
    @Length(min = 7, max = 40)
    @Schema(description = "이메일(계정)", minLength = 7,maxLength = 40)
    private String email;

    @NotNull
//    @Length(min = 8, max = 20)
    @Schema(description = "비밀번호")
    private String password;

    @NotNull
    @Length(min = 13, max = 20)
    @Schema(description = "핸드폰 번호", minLength = 13,maxLength = 20)
    private String phoneNumber;

    @NotNull
    @Schema(description = "기기 종류")
    private PhoneType phoneType;

    @NotNull
    @Length(min = 1, max = 20)
    @Schema(description = "계좌주", minLength = 1,maxLength = 20)
    private String bankOwner;

    @NotNull
    @Schema(description = "은행 이름")
    private BankName bankName;

    @NotNull
    @Length(min = 10, max = 14)
    @Schema(description = "계좌주 주민번호", minLength = 10,maxLength = 14)
    private String bankIdNum;

    @NotNull
    @Length(min = 1, max = 30)
    @Schema(description = "계좌 번호", minLength = 1,maxLength = 30)
    private String bankNumber;

    @NotNull
    @Schema(description = "배달 희망 지역")
    private AddressWish addressWish;

    @NotNull
    @Schema(description = "배달 수단")
    private DriveType driveType;

    @NotNull
    @Length(min = 6, max = 20)
    @Schema(description = "번호판", minLength = 6,maxLength = 20)
    private String driveNumber;

    @NotNull
    @Schema(description = "정지 상태")
    private Boolean isBan;

    @Schema(description = "정지 사유")
    private ReasonBan reasonBan;

    @Schema(description = "정지일")
    private LocalDateTime dateBan;

    @Schema(description = "비고")
    private String etcMemo;
}
