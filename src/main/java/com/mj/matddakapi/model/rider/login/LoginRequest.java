package com.mj.matddakapi.model.rider.login;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

@Getter
@Setter
public class LoginRequest {
    @NotNull
    @Length(min = 7, max = 40)
    @Schema(description = "이메일(계정)", minLength = 7,maxLength = 40)
    private String Email;

    @NotNull
//    @Length(min = 8, max = 20)
    @Schema(description = "비밀번호", minLength = 8, maxLength = 20)
    private String Password;
}
