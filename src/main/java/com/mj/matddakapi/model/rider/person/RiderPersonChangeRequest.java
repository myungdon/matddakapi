package com.mj.matddakapi.model.rider.person;

import com.mj.matddakapi.enums.rider.AddressWish;
import com.mj.matddakapi.enums.rider.BankName;
import com.mj.matddakapi.enums.rider.DriveType;
import com.mj.matddakapi.enums.rider.PhoneType;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

@Getter
@Setter
public class RiderPersonChangeRequest {
//    @NotNull
//    @Length(min = 7, max = 40)
//    @Schema(description = "이메일(계정)", minLength = 7,maxLength = 40)
//    private String email;
//
//    @NotNull
//    @Length(min = 8, max = 20)
//    @Schema(description = "비밀번호", minLength = 8,maxLength = 20)
//    private String password;
//
//    @NotNull
//    @Length(min = 13, max = 20)
//    @Schema(description = "핸드폰 번호", minLength = 13,maxLength = 20)
//    private String phoneNumber;
//
//    @NotNull
//    @Schema(description = "기기 종류")
//    private PhoneType phoneType;

    @NotNull
    @Schema(description = "이름")
    private String name;

    @NotNull
    @Schema(description = "배달 희망 지역")
    private AddressWish addressWish;

    @NotNull
    @Schema(description = "배달 수단")
    private DriveType driveType;

    @NotNull
    @Schema(description = "번호판")
    private String driveNumber;

    @NotNull
    @Schema(description = "계좌주")
    private String bankOwner;

    @NotNull
    @Schema(description = "은행명")
    private BankName bankName;

    @NotNull
    @Schema(description = "계좌번호")
    private String bankNumber;
}
