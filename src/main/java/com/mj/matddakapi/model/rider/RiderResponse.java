package com.mj.matddakapi.model.rider;

import com.mj.matddakapi.entity.Rider;
import com.mj.matddakapi.enums.rider.AddressWish;
import com.mj.matddakapi.enums.rider.BankName;
import com.mj.matddakapi.enums.rider.DriveType;
import com.mj.matddakapi.interfaces.CommonModelBuilder;
import com.mj.matddakapi.model.attendance.AttendanceResponse;
import com.mj.matddakapi.model.riderpay.RiderPayResponse;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.time.LocalDate;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class RiderResponse {
    @Schema(description = "시퀀스")
    private Long id;

    @Schema(description = "이름")
    private String name;

    @Schema(description = "회원 등급")
    private String Admin;

    @Schema(description = "주민 번호")
    private String idNum;

    @Schema(description = "부모님 동의서")
    private String confirmParents;

    @Schema(description = "이메일(계정)")
    private String email;

    @Schema(description = "비밀번호")
    private String password;

    @Schema(description = "핸드폰 번호")
    private String phoneNumber;

    @Schema(description = "기기 종류")
    private String phoneType;

    @Schema(description = "계좌주")
    private String bankOwner;

    @Schema(description = "은행 이름")
    private String bankName;

    private BankName bankValue;

    @Schema(description = "계좌주 주민번호")
    private String bankIdNum;

    @Schema(description = "계좌번호")
    private String bankNumber;

    @Schema(description = "배달 희망 지역")
    private String addressWish;

    private AddressWish addressWishValue;

    @Schema(description = "배달 수단")
    private String driveType;

    private DriveType driveTypeValue;

    @Schema(description = "번호판")
    private String driveNumber;

    @Schema(description = "가입일")
    private String dateJoin;

    @Schema(description = "정지 상태")
    private String isBan;

    @Schema(description = "정지 사유")
    private String reasonBan;

    @Schema(description = "정지일")
    private String dateBan;

    @Schema(description = "비고")
    private String etcMemo;

    @Schema(description = "근태")
    private AttendanceResponse attendance;

    @Schema(description = "보유페이")
    private RiderPayResponse riderPay;

    public RiderResponse(Builder builder) {
        this.id = builder.id;
        this.name = builder.name;
        this.Admin = builder.Admin;
        this.idNum = builder.idNum;
        this.confirmParents = builder.confirmParents;
        this.email = builder.email;
        this.password = builder.password;
        this.phoneNumber = builder.phoneNumber;
        this.phoneType = builder.phoneType;
        this.bankOwner = builder.bankOwner;
        this.bankName = builder.bankName;
        this.bankIdNum = builder.bankIdNum;
        this.bankNumber = builder.bankNumber;
        this.addressWish = builder.addressWish;
        this.driveType = builder.driveType;
        this.driveNumber = builder.driveNumber;
        this.dateJoin = builder.dateJoin;
        this.isBan = builder.isBan;
        this.reasonBan = builder.reasonBan;
        this.dateBan = builder.dateBan;
        this.etcMemo = builder.etcMemo;
        this.attendance = builder.attendance;
        this.riderPay = builder.riderPay;
        this.bankValue = builder.bankValue;
        this.addressWishValue = builder.addressWishValue;
        this.driveTypeValue = builder.driveTypeValue;
    }

    public static class Builder implements CommonModelBuilder<RiderResponse> {
        private final Long id;

        private final String name;

        private final String Admin;

        private final String idNum;

        private final String confirmParents;

        private final String email;

        private final String password;

        private final String phoneNumber;

        private final String phoneType;

        private final String bankOwner;

        private final String bankName;
        private final String bankIdNum;

        private final String bankNumber;

        private final String addressWish;

        private final String driveType;

        private final String driveNumber;

        private final String dateJoin;

        private final String isBan;

        private final String reasonBan;

        private final String dateBan;

        private final String etcMemo;
        private final AttendanceResponse attendance;
        private final RiderPayResponse riderPay;
        private final BankName bankValue;
        private final AddressWish addressWishValue;
        private final DriveType driveTypeValue;


        public Builder(Rider rider, AttendanceResponse attendance, RiderPayResponse riderPay) {

            this.id = rider.getId();
            this.name = rider.getName();
            this.Admin = rider.getAdmin().getName();
            this.idNum = rider.getIdNum();
            this.confirmParents = rider.getConfirmParents();
            this.email = rider.getEmail();
            this.password = rider.getPassword();
            this.phoneNumber = rider.getPhoneNumber();
            this.phoneType = rider.getPhoneType().getName();
            this.bankOwner = rider.getBankOwner();
            this.bankName = rider.getBankName().getName();
            this.bankIdNum = rider.getBankIdNum();
            this.bankNumber = rider.getBankNumber();
            this.addressWish = rider.getAddressWish().getName();
            this.driveType = rider.getDriveType().getName();
            this.driveNumber = rider.getDriveNumber();
            this.dateJoin = rider.getDateJoin().toString();
            this.isBan = rider.getIsBan() ? "정지" : "활동중";
            this.reasonBan = rider.getReasonBan().getName();
            this.dateBan = rider.getDateBan() == null ? "-" : rider.getDateBan().toString();
            this.etcMemo = rider.getEtcMemo() == null ? "-" : rider.getEtcMemo();
            this.attendance = attendance;
            this.riderPay = riderPay;
            this.bankValue = rider.getBankName();
            this.addressWishValue = rider.getAddressWish();
            this.driveTypeValue = rider.getDriveType();
        }

        @Override
        public RiderResponse build() {
            return new RiderResponse(this);
        }
    }
}
