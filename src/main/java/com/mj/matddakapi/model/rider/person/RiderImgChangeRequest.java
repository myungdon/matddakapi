package com.mj.matddakapi.model.rider.person;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

@Getter
@Setter
public class RiderImgChangeRequest {
    @NotNull
    @Length(min = 1, max = 100)
    @Schema(description = "라이더 프사", minLength = 1, maxLength = 100)
    private String riderImg;
}
