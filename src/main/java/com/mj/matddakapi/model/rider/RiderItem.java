package com.mj.matddakapi.model.rider;

import com.mj.matddakapi.entity.Rider;
import com.mj.matddakapi.interfaces.CommonModelBuilder;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)

public class RiderItem {
    @Schema(description = "시퀀스")
    private Long id;

    @Schema(description = "이름")
    private String name;

    @Schema(description = "회원 등급")
    private String admin;

    @Schema(description = "주민 번호")
    private String idNum;

    @Schema(description = "부모님 동의서")
    private String confirmParents;

    @Schema(description = "이메일(계정)")
    private String email;

    @Schema(description = "비밀번호")
    private String password;

    @Schema(description = "핸드폰 번호")
    private String phoneNumber;

    @Schema(description = "기기 종류")
    private String phoneType;

    @Schema(description = "계좌주")
    private String bankOwner;

    @Schema(description = "은행 이름")
    private String bankName;

    @Schema(description = "계좌주 주민번호")
    private String bankIdNum;

    @Schema(description = "계좌번호")
    private String bankNumber;

    @Schema(description = "배달 희망 지역")
    private String addressWish;

    @Schema(description = "배달 수단")
    private String driveType;

    @Schema(description = "번호판")
    private String driveNumber;

    @Schema(description = "가입일")
    private String dateJoin;

    @Schema(description = "정지 상태")
    private String isBan;

    @Schema(description = "정지 사유")
    private String reasonBan;

    @Schema(description = "정지일")
    private String dateBan;

    @Schema(description = "비고")
    private String etcMemo;

    private RiderItem(Builder builder) {
        this.id = builder.id;
        this.name = builder.name;
        this.admin = builder.admin;
        this.idNum = builder.idNum;
        this.confirmParents = builder.confirmParents;
        this.email = builder.email;
        this.password = builder.password;
        this.phoneNumber = builder.phoneNumber;
        this.phoneType = builder.phoneType;
        this.bankOwner = builder.bankOwner;
        this.bankName = builder.bankName;
        this.bankIdNum = builder.bankIdNum;
        this.bankNumber = builder.bankNumber;
        this.addressWish = builder.addressWish;
        this.driveType = builder.driveType;
        this.driveNumber = builder.driveNumber;
        this.dateJoin = builder.dateJoin;
        this.isBan = builder.isBan;
        this.reasonBan = builder.reasonBan;
        this.dateBan = builder.dateBan;
        this.etcMemo = builder.etcMemo;
    }


    public static class Builder implements CommonModelBuilder<RiderItem> {
        private final Long id;

        private final String name;

        private final String admin;

        private final String idNum;

        private final String confirmParents;

        private final String email;

        private final String password;

        private final String phoneNumber;

        private final String phoneType;

        private final String bankOwner;

        private final String bankName;

        private final String bankIdNum;

        private final String bankNumber;

        private final String addressWish;

        private final String driveType;

        private final String driveNumber;

        private final String dateJoin;

        private final String isBan;

        private final String reasonBan;

        private final String dateBan;

        private final String etcMemo;


        public Builder(Rider rider) {
            this.id = rider.getId();
            this.name = rider.getName();
            this.admin = rider.getAdmin().getName();
            this.idNum = rider.getIdNum();
            this.confirmParents = rider.getConfirmParents();
            this.email = rider.getEmail();
            this.password = rider.getPassword();
            this.phoneNumber = rider.getPhoneNumber();
            this.phoneType = rider.getPhoneType().getName();
            this.bankOwner = rider.getBankOwner();
            this.bankName = rider.getBankName().getName();
            this.bankNumber = rider.getBankNumber();
            this.bankIdNum = rider.getBankIdNum();
            this.addressWish = rider.getAddressWish().getName();
            this.driveType = rider.getDriveType().getName();
            this.driveNumber = rider.getDriveNumber();
            this.dateJoin = rider.getDateJoin().toString();
            this.isBan = rider.getIsBan() ? "정지" : "활동중";
            this.reasonBan = rider.getReasonBan().getName();
            this.dateBan = rider.getDateBan() == null ? "-" : rider.getDateBan().toString();
            this.etcMemo = rider.getEtcMemo() == null ? "-" : rider.getEtcMemo();
        }

        @Override
        public RiderItem build() {
            return new RiderItem(this);
        }
    }
}
