package com.mj.matddakapi.model.rider.person;

import com.mj.matddakapi.enums.rider.PhoneType;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RiderPhoneTypeChangeRequest {
    @NotNull
    @Schema(description = "기기 종류")
    private PhoneType phoneType;
}
