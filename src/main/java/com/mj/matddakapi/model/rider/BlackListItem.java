package com.mj.matddakapi.model.rider;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BlackListItem {
    @Schema(description = "이름")
    private String name;

    @Schema(description = "핸드폰 번호")
    private String phoneNumber;

    @Schema(description = "정지 상태")
    private String isBan;

    @Schema(description = "정지 사유")
    private String reasonBan;

    @Schema(description = "정지일")
    private String dateBan;
}
