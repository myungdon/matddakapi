package com.mj.matddakapi.model.rider.person;

import com.mj.matddakapi.enums.rider.BankName;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

@Getter
@Setter
public class RiderBankChangeRequest {
    @NotNull
    @Length(min = 1, max = 20)
    @Schema(description = "계좌주", minLength = 1,maxLength = 20)
    private String bankOwner;

    @NotNull
    @Schema(description = "은행 이름")
    private BankName bankName;

    @NotNull
    @Length(min = 10, max = 14)
    @Schema(description = "계좌주 주민번호", minLength = 10,maxLength = 14)
    private String bankIdNum;

    @NotNull
    @Length(min = 1, max = 30)
    @Schema(description = "계좌 번호", minLength = 1,maxLength = 30)
    private String bankNumber;
}
