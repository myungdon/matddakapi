package com.mj.matddakapi.model.rider.person;

import com.mj.matddakapi.enums.rider.DriveType;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

@Getter
@Setter

public class RiderTypeChangeRequest {
    @NotNull
    @Schema(description = "배달 수단")
    private DriveType driveType;

    @NotNull
    @Length(min = 6, max = 20)
    @Schema(description = "번호판", minLength = 6,maxLength = 20)
    private String driveNumber;
}
