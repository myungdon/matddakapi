package com.mj.matddakapi.model.delivery;

import com.mj.matddakapi.entity.Delivery;
import com.mj.matddakapi.interfaces.CommonModelBuilder;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.time.format.DateTimeFormatter;


@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class DeliveryItem {
    @Schema(description = "시퀀스")
    private Long id;

    @Schema(description = "라이더 ID")
    private Long riderId;

    @Schema(description = "라이더 이름")
    private String riderName;

    @Schema(description = "주문 ID")
    private Long askId;

    @Schema(description = "가게 이름")
    private String storeName;

    @Schema(description = "가게 주소(도로명)")
    private String addressStoreDo;

    @Schema(description = "고객 주소(도로명)")
    private String addressClientDo;

    @Schema(description = "배송 상태")
    private String stateDelivery;

    @Schema(description = "배송 거리")
    private Double distance;

    @Schema(description = "배달비")
    private Double feeTotal;

    @Schema(description = "선결제 여부")
    private String isPay;

    @Schema(description = "라이더 요청사항")
    private String requestRider;

    @Schema(description = "배차 시간")
    private String datePick;

    @Schema(description = "출발 시간")
    private String dateGo;

    @Schema(description = "완료 시간")
    private String dateDone;

    @Schema(description = "취소 시간")
    private String dateCancel;

    public DeliveryItem(Builder builder) {
        this.id = builder.id;
        this.riderId = builder.riderId;
        this.riderName = builder.riderName;
        this.askId = builder.askId;
        this.storeName = builder.storeName;
        this.addressStoreDo = builder.addressStoreDo;
        this.addressClientDo = builder.addressClientDo;
        this.stateDelivery = builder.stateDelivery;
        this.distance = builder.distance;
        this.feeTotal = builder.feeTotal;
        this.isPay = builder.isPay;
        this.requestRider = builder.requestRider;
        this.datePick = builder.datePick;
        this.dateGo = builder.dateGo;
        this.dateDone = builder.dateDone;
        this.dateCancel = builder.dateCancel;
    }

    public static class Builder implements CommonModelBuilder<DeliveryItem> {
        private final Long id;
        private final Long riderId;
        private final String riderName;
        private final Long askId;
        private final String storeName;
        private final String addressStoreDo;
        private final String addressClientDo;
        private final String stateDelivery;
        private final Double distance;
        private final Double feeTotal;
        private final String isPay;
        private final String requestRider;
        private final String datePick;
        private final String dateGo;
        private final String dateDone;
        private final String dateCancel;

        public Builder(Delivery delivery) {
            this.id = delivery.getId();
            this.riderId = delivery.getRider().getId();
            this.riderName = delivery.getRider().getName();
            this.askId = delivery.getAsk().getId();
            this.storeName = delivery.getAsk().getStore().getStoreName();
            this.addressStoreDo = delivery.getAsk().getStore().getAddressStoreDo();
            this.addressClientDo = delivery.getAsk().getAddressClientDo();
            this.stateDelivery = delivery.getStateDelivery().getName();
            this.distance = delivery.getAsk().getDistance();
            this.feeTotal = (double) Math.round(delivery.getAsk().getPriceRide() * 0.6);
            this.isPay = delivery.getAsk().getIsPay() ? "선" : "후";
            this.requestRider = delivery.getAsk().getRequestRider();
            this.datePick = delivery.getDatePick() == null ? null : delivery.getDatePick().format(DateTimeFormatter.ofPattern("yyyy년 MM월 dd일"));
            this.dateGo = delivery.getDateGo() == null ? null : delivery.getDateGo().format(DateTimeFormatter.ofPattern("yyyy년 MM월 dd일"));
            this.dateDone = delivery.getDateDone() == null ? null : delivery.getDateDone().format(DateTimeFormatter.ofPattern("yyyy년 MM월 dd일"));
            this.dateCancel = delivery.getDateCancel() == null ? null : delivery.getDateCancel().format(DateTimeFormatter.ofPattern("yyyy년 MM월 dd일"));
        }

        @Override
        public DeliveryItem build() {
            return new DeliveryItem(this);
        }
    }
}
