package com.mj.matddakapi.model.delivery;

import com.mj.matddakapi.entity.Delivery;
import com.mj.matddakapi.interfaces.CommonModelBuilder;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.format.DateTimeFormatter;


@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class DeliveryResponse {
    @Schema(description = "시퀀스")
    private Long id;

    @Schema(description = "라이더 ID")
    private Long riderId;

    @Schema(description = "라이더 이름")
    private String riderName;

    @Schema(description = "주문 ID")
    private Long askId;

    @Schema(description = "가게 이름")
    private String storeName;

    @Schema(description = "가게 주소(도로명)")
    private String addressStoreDo;

    @Schema(description = "고객 주소(도로명)")
    private String addressClientDo;

    @Schema(description = "가게 좌표 위도")
    private Double storeX;

    @Schema(description = "가게 좌표 경도")
    private Double storeY;

    @Schema(description = "고객 좌표 위도")
    private Double clientX;

    @Schema(description = "고객 좌표 경도")
    private Double clientY;

    @Schema(description = "배송 상태")
    private String stateDelivery;

    @Schema(description = "배송 거리")
    private Double distance;

    @Schema(description = "음식 가격")
    private Double priceFood;

    @Schema(description = "배달비")
    private Double feeTotal;

    @Schema(description = "선결제 여부")
    private String isPay;

    @Schema(description = "라이더 요청사항")
    private String requestRider;

    @Schema(description = "배차 시간")
    private String datePick;

    @Schema(description = "출발 시간")
    private String dateGo;

    @Schema(description = "완료 시간")
    private String dateDone;

    @Schema(description = "취소 시간")
    private String dateCancel;

    @Schema(description = "주문 메뉴")
    private String askMenu;

    @Schema(description = "가게 요청사항")
    private String requestStore;

    @Schema(description = "결제 금액")
    private Double priceTotal;

    @Schema(description = "관리자가 받는 수수료")
    private Double feeAdmin;

    @Schema(description = "배달수단")
    private String driveType;

    @Schema(description = "라이더 핸드폰번호")
    private String phoneRider;

    @Schema(description = "가게 번호")
    private String storeNumber;

    public DeliveryResponse(Builder builder) {
        this.id = builder.id;
        this.riderId = builder.riderId;
        this.riderName = builder.riderName;
        this.askId = builder.askId;
        this.storeName = builder.storeName;
        this.addressStoreDo = builder.addressStoreDo;
        this.addressClientDo = builder.addressClientDo;
        this.storeX = builder.storeX;
        this.storeY = builder.storeY;
        this.clientX = builder.clientX;
        this.clientY = builder.clientY;
        this.stateDelivery = builder.stateDelivery;
        this.distance = builder.distance;
        this.priceFood = builder.priceFood;
        this.feeTotal = builder.feeTotal;
        this.isPay = builder.isPay;
        this.requestRider = builder.requestRider;
        this.datePick = builder.datePick;
        this.dateGo = builder.dateGo;
        this.dateDone = builder.dateDone;
        this.dateCancel = builder.dateCancel;
        this.askMenu = builder.askMenu;
        this.requestStore = builder.requestStore;
        this.priceTotal = builder.priceTotal;
        this.feeAdmin = builder.feeAdmin;
        this.driveType = builder.driveType;
        this.phoneRider = builder.phoneRider;
        this.storeNumber = builder.storeNumber;
    }

    public static class Builder implements CommonModelBuilder<DeliveryResponse> {
        private final Long id;
        private final Long riderId;
        private final String riderName;
        private final Long askId;
        private final String storeName;
        private final String addressStoreDo;
        private final String addressClientDo;
        private final Double storeX;
        private final Double storeY;
        private final Double clientX;
        private final Double clientY;
        private final String stateDelivery;
        private final Double distance;
        private final Double priceFood;
        private final Double feeTotal;
        private final String isPay;
        private final String requestRider;
        private final String datePick;
        private final String dateGo;
        private final String dateDone;
        private final String dateCancel;
        private final String askMenu;
        private final String requestStore;
        private final Double priceTotal;
        private final Double feeAdmin;
        private final String driveType;
        private final String phoneRider;
        private final String storeNumber;

        public Builder(Delivery delivery) {
            this.id = delivery.getId();
            this.riderId = delivery.getRider().getId();
            this.riderName = delivery.getRider().getName();
            this.askId = delivery.getAsk().getId();
            this.storeName = delivery.getAsk().getStore().getStoreName();
            this.addressStoreDo = delivery.getAsk().getStore().getAddressStoreDo();
            this.addressClientDo = delivery.getAsk().getAddressClientDo();
            this.storeX = delivery.getAsk().getStore().getStoreX();
            this.storeY = delivery.getAsk().getStore().getStoreY();
            this.clientX = delivery.getAsk().getClientX();
            this.clientY = delivery.getAsk().getClientY();
            this.stateDelivery = delivery.getStateDelivery().getName();
            this.distance = delivery.getAsk().getDistance();
            this.priceFood = delivery.getAsk().getPriceFood();
            this.feeTotal = (double) Math.round(delivery.getAsk().getPriceRide() * 0.6);
            this.isPay = delivery.getAsk().getIsPay() ? "선" : "후";
            this.requestRider = delivery.getAsk().getRequestRider();
            this.datePick = delivery.getDatePick() == null ? null : delivery.getDatePick().format(DateTimeFormatter.ofPattern("yyyy년 MM월 dd일"));
            this.dateGo = delivery.getDateGo() == null ? null : delivery.getDateGo().format(DateTimeFormatter.ofPattern("yyyy년 MM월 dd일"));
            this.dateDone = delivery.getDateDone() == null ? null : delivery.getDateDone().format(DateTimeFormatter.ofPattern("yyyy년 MM월 dd일"));
            this.dateCancel = delivery.getDateCancel() == null ? null : delivery.getDateCancel().format(DateTimeFormatter.ofPattern("yyyy년 MM월 dd일"));
            this.askMenu = delivery.getAsk().getAskMenu();
            this.requestStore = delivery.getAsk().getRequestStore();
            this.priceTotal = delivery.getAsk().getPriceTotal();
            this.feeAdmin = delivery.getAsk().getPriceRide() * 0.6 * 0.1;
            this.driveType = delivery.getRider().getDriveType().getName();
            this.phoneRider = delivery.getRider().getPhoneNumber();
            this.storeNumber = delivery.getAsk().getStore().getStoreNumber();
        }

        @Override
        public DeliveryResponse build() {
            return new DeliveryResponse(this);
        }
    }
}
