package com.mj.matddakapi.model.delivery;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TabCountResponse {
    @Schema(description = "요청 탭 갯수")
    private Long requestCount;

    @Schema(description = "배차 탭 갯수")
    private Long pickCount;

    @Schema(description = "출발 탭 갯수")
    private Long goCount;

    @Schema(description = "완료 탭 갯수")
    private Long doneCount;

    @Schema(description = "취소 탭 갯수")
    private Long cancelCount;
}
