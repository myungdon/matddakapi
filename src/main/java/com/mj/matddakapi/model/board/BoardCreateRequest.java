package com.mj.matddakapi.model.board;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

@Getter
@Setter
public class BoardCreateRequest {
    @NotNull
    @Length(min = 2, max = 50)
    @Schema(description = "게시글 제목", minLength = 2, maxLength = 50)
    private String title;


    @NotNull
    @Schema(description = "게시글 내용")
    private String text;
}
