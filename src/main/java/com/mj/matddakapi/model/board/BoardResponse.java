package com.mj.matddakapi.model.board;

import com.mj.matddakapi.entity.Board;
import com.mj.matddakapi.interfaces.CommonModelBuilder;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class BoardResponse {
    @Schema(description = "시퀀스")
    private Long id;

    @Schema(description = "게시글 제목")
    private String title;

    @Schema(description = "게시글 첨부 이미지")
    private String img;

    @Schema(description = "게시글 내용")
    private String text;

    @Schema(description = "작성일시")
    private String dateCreate;

    public BoardResponse(Builder builder) {
        this.id = builder.id;
        this.title = builder.title;
        this.img = builder.img;
        this.text = builder.text;
        this.dateCreate = builder.dateCreate;
    }

    public static class Builder implements CommonModelBuilder<BoardResponse> {
        private final Long id;
        private final String title;
        private final String img;
        private final String text;
        private final String dateCreate;

        public Builder(Board board) {
            this.id = board.getId();
            this.title = board.getTitle();
            this.img = board.getImg();
            this.text = board.getText();
            this.dateCreate = board.getDateCreate().toString();
        }
        @Override
        public BoardResponse build() {
            return new BoardResponse(this);
        }
    }
}
