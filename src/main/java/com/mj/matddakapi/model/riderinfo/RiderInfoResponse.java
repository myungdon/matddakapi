package com.mj.matddakapi.model.riderinfo;

import com.mj.matddakapi.entity.RiderInfo;
import com.mj.matddakapi.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class RiderInfoResponse {
    private String riderName;
    private String isWork;
    private Double payNow;
    private String addressWish;
    private String riderImg;

    public RiderInfoResponse(Builder builder){
        this.riderName = builder.riderName;
        this.isWork = builder.isWork;
        this.payNow = builder.payNow;
        this.addressWish = builder.addressWish;
        this.riderImg = builder.riderImg;
    }

    public static class Builder implements CommonModelBuilder<RiderInfoResponse> {
        private final String riderName;
        private final String isWork;
        private final Double payNow;
        private final String addressWish;
        private final String riderImg;

        public Builder(RiderInfo riderInfo) {
            this.riderName = riderInfo.getRiderPay().getRider().getName();
            this.isWork = riderInfo.getAttendance().getIsWork() ? "정상업무" : "휴식";
            this.payNow = riderInfo.getRiderPay().getPayNow();
            this.addressWish = riderInfo.getRiderPay().getRider().getAddressWish().getName();
            this.riderImg = riderInfo.getRiderPay().getRider().getRiderImg();
        }

        @Override
        public RiderInfoResponse build() {
            return new RiderInfoResponse(this);
        }
    }
}
