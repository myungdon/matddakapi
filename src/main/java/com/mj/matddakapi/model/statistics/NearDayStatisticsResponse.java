package com.mj.matddakapi.model.statistics;

import com.mj.matddakapi.interfaces.CommonModelBuilder;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@Getter

@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class NearDayStatisticsResponse {
    @Schema(description = "가입일 리스트")
    private List<String> labels;

    @Schema(description = "일일 가입 회원수 리스트")
    private List<Long> datasets;

    public NearDayStatisticsResponse(Builder builder) {
        this.labels = builder.labels;
        this.datasets = builder.datasets;
    }

    public static class Builder implements CommonModelBuilder<NearDayStatisticsResponse> {
        private List<String> labels;
        private List<Long> datasets;

        public Builder(List<String> labels, List<Long> datasets) {
            this.labels = labels;
            this.datasets = datasets;
        }

        public Builder labels(List<String> labels) {
            this.labels = labels;
            return this;
        }
        public Builder datasets(List<Long> datasets) {
            this.datasets = datasets;
            return this;
        }

        @Override
        public NearDayStatisticsResponse build() {
            return new NearDayStatisticsResponse(this);
        }
    }
}
