package com.mj.matddakapi.model.statistics;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IncomeStatisticsResponse {
    @Schema(description = "날짜")
    private String dateTotalIncome;

    @Schema(description = "배달 횟수")
    private Integer totalCount;

    @Schema(description = "배달비 총합")
    private Double totalFee;

    @Schema(description = "관리자 수익총합")
    private Double totalAdmin;

    @Schema(description = "라이더 수익총합")
    private Double totalRider;

    @Schema(description = "총 이동거리")
    private Double totalDistance;

    @Schema(description = "평균 이동거리")
    private Double perDistance;

    @Schema(description = "요청 취소 횟수")
    private Integer totalCancelCount;
}
