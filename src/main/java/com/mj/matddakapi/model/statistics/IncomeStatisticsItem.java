package com.mj.matddakapi.model.statistics;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IncomeStatisticsItem {
    @Schema(description = "날짜")
    private String dateTotalIncome;

    @Schema(description = "라이더 수익총합")
    private Double totalRider;

    @Schema(description = "라이더 일별수익")
    private Double totalDayRider;

    @Schema(description = "배달 횟수")
    private Integer totalCount;

    @Schema(description = "요청 취소 횟수")
    private Integer totalCancelCount;
}
