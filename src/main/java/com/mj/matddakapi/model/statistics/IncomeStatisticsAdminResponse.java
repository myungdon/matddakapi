package com.mj.matddakapi.model.statistics;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IncomeStatisticsAdminResponse {
    @Schema(description = "일간 수익 날짜")
    private String dateDayIncome;

    @Schema(description = "일간 수익")
    private Integer totalDayAdmin;

    @Schema(description = "주간 수익 날짜")
    private String dateWeekIncome;

    @Schema(description = "주간 수익")
    private Integer totalWeekAdmin;

    @Schema(description = "월간 수익 날짜")
    private String dateMonthIncome;

    @Schema(description = "월간 수익")
    private Integer totalMonthAdmin;
}
