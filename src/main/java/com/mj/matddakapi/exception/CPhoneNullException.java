package com.mj.matddakapi.exception;

public class CPhoneNullException extends RuntimeException{
    public CPhoneNullException(String msg, Throwable t) {
        super(msg, t);
    }

    public CPhoneNullException(String msg) {
        super(msg);
    }

    public CPhoneNullException() {
        super();
    }
}
