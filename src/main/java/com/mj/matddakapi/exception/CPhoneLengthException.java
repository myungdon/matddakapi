package com.mj.matddakapi.exception;

public class CPhoneLengthException extends RuntimeException{
    public CPhoneLengthException(String msg, Throwable t) {
        super(msg, t);
    }

    public CPhoneLengthException(String msg) {
        super(msg);
    }

    public CPhoneLengthException() {
        super();
    }
}
