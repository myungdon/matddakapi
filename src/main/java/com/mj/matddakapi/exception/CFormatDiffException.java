package com.mj.matddakapi.exception;

public class CFormatDiffException extends RuntimeException{
    public CFormatDiffException(String msg, Throwable t) {
        super(msg, t);
    }

    public CFormatDiffException(String msg) {
        super(msg);
    }

    public CFormatDiffException() {
        super();
    }
}
