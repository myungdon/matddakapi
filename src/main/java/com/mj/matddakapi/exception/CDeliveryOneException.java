package com.mj.matddakapi.exception;

public class CDeliveryOneException extends RuntimeException{
    public CDeliveryOneException(String msg, Throwable t) {
        super(msg, t);
    }

    public CDeliveryOneException(String msg) {
        super(msg);
    }

    public CDeliveryOneException() {
        super();
    }
}
