package com.mj.matddakapi.exception;

public class CDeliveryDoneException extends RuntimeException{
    public CDeliveryDoneException(String msg, Throwable t) {
        super(msg, t);
    }

    public CDeliveryDoneException(String msg) {
        super(msg);
    }

    public CDeliveryDoneException() {
        super();
    }
}
