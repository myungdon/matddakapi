package com.mj.matddakapi.exception;

public class CEmailSameException extends RuntimeException{
    public CEmailSameException(String msg, Throwable t) {
        super(msg, t);
    }

    public CEmailSameException(String msg) {
        super(msg);
    }

    public CEmailSameException() {
        super();
    }
}
