package com.mj.matddakapi.exception;

public class CAgeUnderException extends RuntimeException{
    public CAgeUnderException(String msg, Throwable t) {
        super(msg, t);
    }

    public CAgeUnderException(String msg) {
        super(msg);
    }

    public CAgeUnderException() {
        super();
    }
}
