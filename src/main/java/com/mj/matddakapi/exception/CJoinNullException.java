package com.mj.matddakapi.exception;

public class CJoinNullException extends RuntimeException{
    public CJoinNullException(String msg, Throwable t) {
        super(msg, t);
    }

    public CJoinNullException(String msg) {
        super(msg);
    }

    public CJoinNullException() {
        super();
    }
}
