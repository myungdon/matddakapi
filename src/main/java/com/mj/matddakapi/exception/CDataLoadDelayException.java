package com.mj.matddakapi.exception;

public class CDataLoadDelayException extends RuntimeException{
    public CDataLoadDelayException(String msg, Throwable t) {
        super(msg, t);
    }

    public CDataLoadDelayException(String msg) {
        super(msg);
    }

    public CDataLoadDelayException() {
        super();
    }
}
