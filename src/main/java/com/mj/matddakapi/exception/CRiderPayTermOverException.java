package com.mj.matddakapi.exception;

public class CRiderPayTermOverException extends RuntimeException{
    public CRiderPayTermOverException(String msg, Throwable t) {
        super(msg, t);
    }

    public CRiderPayTermOverException(String msg) {
        super(msg);
    }

    public CRiderPayTermOverException() {
        super();
    }
}
