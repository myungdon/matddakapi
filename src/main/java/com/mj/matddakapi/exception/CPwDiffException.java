package com.mj.matddakapi.exception;

public class CPwDiffException extends RuntimeException{
    public CPwDiffException(String msg, Throwable t) {
        super(msg, t);
    }

    public CPwDiffException(String msg) {
        super(msg);
    }

    public CPwDiffException() {
        super();
    }
}
