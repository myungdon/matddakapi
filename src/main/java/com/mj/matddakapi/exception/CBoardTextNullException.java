package com.mj.matddakapi.exception;

public class CBoardTextNullException extends RuntimeException{
    public CBoardTextNullException(String msg, Throwable t) {
        super(msg, t);
    }

    public CBoardTextNullException(String msg) {
        super(msg);
    }

    public CBoardTextNullException() {
        super();
    }
}
