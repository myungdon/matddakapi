package com.mj.matddakapi.exception;

public class CIdNumLengthException extends RuntimeException{
    public CIdNumLengthException(String msg, Throwable t) {
        super(msg, t);
    }

    public CIdNumLengthException(String msg) {
        super(msg);
    }

    public CIdNumLengthException() {
        super();
    }
}
