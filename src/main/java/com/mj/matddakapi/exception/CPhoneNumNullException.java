package com.mj.matddakapi.exception;

public class CPhoneNumNullException extends RuntimeException{
    public CPhoneNumNullException(String msg, Throwable t) {
        super(msg, t);
    }

    public CPhoneNumNullException(String msg) {
        super(msg);
    }

    public CPhoneNumNullException() {
        super();
    }
}
