package com.mj.matddakapi.exception;

public class CRiderInfoSameException extends RuntimeException{
    public CRiderInfoSameException(String msg, Throwable t) {
        super(msg, t);
    }

    public CRiderInfoSameException(String msg) {
        super(msg);
    }

    public CRiderInfoSameException() {
        super();
    }
}
