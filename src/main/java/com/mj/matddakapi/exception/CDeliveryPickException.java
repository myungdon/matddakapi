package com.mj.matddakapi.exception;

public class CDeliveryPickException extends RuntimeException{
    public CDeliveryPickException(String msg, Throwable t) {
        super(msg, t);
    }

    public CDeliveryPickException(String msg) {
        super(msg);
    }

    public CDeliveryPickException() {
        super();
    }
}
