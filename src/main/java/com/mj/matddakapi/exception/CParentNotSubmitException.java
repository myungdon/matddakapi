package com.mj.matddakapi.exception;

public class CParentNotSubmitException extends RuntimeException{
    public CParentNotSubmitException(String msg, Throwable t) {
        super(msg, t);
    }

    public CParentNotSubmitException(String msg) {
        super(msg);
    }

    public CParentNotSubmitException() {
        super();
    }
}
