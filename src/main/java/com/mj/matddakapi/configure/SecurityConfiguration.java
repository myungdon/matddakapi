package com.mj.matddakapi.configure;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.List;

@Configuration
@EnableWebSecurity
@EnableMethodSecurity
@RequiredArgsConstructor
public class SecurityConfiguration {
    private final JwtTokenProvider jwtTokenProvider;
    private final CustomAuthenticationEntryPoint entryPoint;
    private final CustomAccessDeniedHandler accessDenied;

    @Bean
    public CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration config = new CorsConfiguration();

        config.setAllowCredentials(true);
        config.setAllowedOriginPatterns(List.of("*"));
        config.setAllowedMethods(List.of("GET", "POST", "PUT", "DELETE", "PATCH", "OPTIONS"));
        config.setAllowedHeaders(List.of("*"));
        config.setExposedHeaders(List.of("*"));

        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", config);
        return source;
    }

    /*
    flutter 에서 header에 token 넣는법
    String? token = await TokenLib.getToken();
    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ' + token!; <-- 한줄 추가

    vuejs 에서 axios에 header 넣는법
    https://velog.io/@ch9eri/Axios-headers-Authorization
     */

    @Bean
    protected SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http.csrf(AbstractHttpConfigurer::disable)
                .formLogin(AbstractHttpConfigurer::disable)
                .httpBasic(AbstractHttpConfigurer::disable)
                .cors(corsConfig -> corsConfig.configurationSource(corsConfigurationSource()))
                .sessionManagement((sessionManagement) ->
                        sessionManagement.sessionCreationPolicy(SessionCreationPolicy.STATELESS))
                .authorizeHttpRequests((authorizeRequests) ->
                        authorizeRequests
                                .requestMatchers("/v3/**", "/swagger-ui/**").permitAll()
                                .requestMatchers(HttpMethod.GET, "exception/**").permitAll()
                                .requestMatchers("/v1/**").permitAll()
                                .requestMatchers("/v1/ask/**").permitAll()
                                .requestMatchers("/v1/attendance/**").permitAll()
                                .requestMatchers("/v1/board/**").permitAll()
                                .requestMatchers("/v1/delivery/**").permitAll()
//                                .requestMatchers(("/v1/delivery/new//ask-id/{askId}")).hasAnyRole("ADMIN")
                                .requestMatchers("/v1/income/**").permitAll()
                                .requestMatchers("/v1/login/**").permitAll()
                                .requestMatchers("/v1/money-history/**").permitAll()
                                .requestMatchers("/v1/pay-history/**").permitAll()
                                .requestMatchers("/v1/rider/**").permitAll()
//                                .requestMatchers("v1/rider/join/**").permitAll()
                                .requestMatchers("/v1/rider-pay/**").permitAll()
                                .requestMatchers("/v1/store/**").permitAll()
                                .requestMatchers("/v1/temp/**").permitAll()
                                .requestMatchers("/v1/gcs/**").permitAll()
                                .requestMatchers("/v1/auth-test/test-admin").hasAnyRole("ADMIN")
                                .requestMatchers("/v1/rider/**").hasAnyRole("RIDER")
                                .anyRequest().hasRole("ADMIN")
                );
        http.exceptionHandling(handler -> handler.accessDeniedHandler(accessDenied));
        http.exceptionHandling(handler -> handler.authenticationEntryPoint(entryPoint));
        http.addFilterBefore(new JwtTokenFilter(jwtTokenProvider), UsernamePasswordAuthenticationFilter.class);

        return http.build();
    }
}
