package com.mj.matddakapi.configure;

import com.mj.matddakapi.enums.ResultCode;
import com.mj.matddakapi.exception.*;
import com.mj.matddakapi.model.generic.CommonResult;
import com.mj.matddakapi.service.ResponseService;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ExceptionAdvice {

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    protected CommonResult defaultException(HttpServletRequest request, Exception e){
        return ResponseService.getFailResult(ResultCode.FAILED);
    }

    @ExceptionHandler(CDeliveryPickException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CDeliveryPickException e){
        return ResponseService.getFailResult(ResultCode.DELIVERY_PICK);
    }
    @ExceptionHandler(CAgeUnderException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customAUException(HttpServletRequest request, CAgeUnderException e){
        return ResponseService.getFailResult(ResultCode.AGE_UNDER);
    }

    @ExceptionHandler(CRiderMissingException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customRMException(HttpServletRequest request, CRiderMissingException e){
        return ResponseService.getFailResult(ResultCode.RIDER_MISSING);
    }

    @ExceptionHandler(CPwDiffException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customPDException(HttpServletRequest request, CPwDiffException e){
        return ResponseService.getFailResult(ResultCode.PASSWORD_DIFFERENT);
    }

    @ExceptionHandler(CEmailFormException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CEmailFormException e){
        return ResponseService.getFailResult(ResultCode.EMAIL_FORM);
    }

    @ExceptionHandler(CPhoneLengthException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CPhoneLengthException e){
        return ResponseService.getFailResult(ResultCode.PHONE_LENGTH);
    }

    @ExceptionHandler(CRiderPayTermOverException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CRiderPayTermOverException e){
        return ResponseService.getFailResult(ResultCode.RIDER_PAY_TERM_OVER);
    }

    @ExceptionHandler(CDeliveryOneException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CDeliveryOneException e){
        return ResponseService.getFailResult(ResultCode.DELIVERY_ONLY_ONE);
    }

    @ExceptionHandler(CMoneyOutOverException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CMoneyOutOverException e){
        return ResponseService.getFailResult(ResultCode.MONEY_OUT_OVER);
    }
}
