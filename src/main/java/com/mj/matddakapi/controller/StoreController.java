package com.mj.matddakapi.controller;

import com.mj.matddakapi.model.generic.CommonResult;
import com.mj.matddakapi.model.generic.ListResult;
import com.mj.matddakapi.model.generic.SingleResult;
import com.mj.matddakapi.model.store.StoreChangeRequest;
import com.mj.matddakapi.model.store.StoreCreateRequest;
import com.mj.matddakapi.model.store.StoreItem;
import com.mj.matddakapi.model.store.StoreResponse;
import com.mj.matddakapi.service.ResponseService;
import com.mj.matddakapi.service.StoreService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/store")
public class StoreController {
    private final StoreService storeService;

    // 가게 정보 등록
    @PostMapping("/new")
    @Operation(summary = "가게 등록")
    public CommonResult setStore(@RequestBody StoreCreateRequest request) {
        storeService.setStore(request);

        return ResponseService.getSuccessResult();
    }

    // 가게 조회 복수
    @GetMapping("/all")
    @Operation(summary = "가게 복수")
    public ListResult<StoreItem> getStores() {
        return ResponseService.getListResult(storeService.getStores(), true);
    }

    // 가게 조회 단수
    @GetMapping("/detail/{storeId}")
    @Operation(summary = "가게 단수")
    public SingleResult<StoreResponse> getStore(@PathVariable long storeId) {
        return ResponseService.getSingleResult(storeService.getStore(storeId));
    }

    // 가게 정보 변경
    @PutMapping("/change/{storeId}")
    @Operation(summary = "가게 변경")
    public CommonResult putStore(@PathVariable long storeId, @RequestBody StoreChangeRequest request) {
        storeService.putStore(storeId, request);

        return ResponseService.getSuccessResult();
    }

    // 가게 정보 삭제
    @DeleteMapping("/delete/{storeId}")
    @Operation(summary = "가게 삭제")
    public CommonResult delStore(@PathVariable long storeId) {
        storeService.delStore(storeId);

        return ResponseService.getSuccessResult();
    }

    // 가게 정보 복수 페이징
    @GetMapping("/all/{pageNum}")
    @Operation(summary = "가게 복수 페이징")
    public ListResult<StoreItem> getStoresP(@PathVariable int pageNum) {
        return ResponseService.getListResult(storeService.getStoresP(pageNum), true);
    }
}