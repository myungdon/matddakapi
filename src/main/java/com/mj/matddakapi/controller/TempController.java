//package com.mj.matddakapi.controller;
//
//import com.mj.matddakapi.model.generic.CommonResult;
//import com.mj.matddakapi.model.generic.SingleResult;
//import com.mj.matddakapi.model.statistics.NearDayStatisticsResponse;
//import com.mj.matddakapi.service.ResponseService;
//import com.mj.matddakapi.service.TempService;
//import io.swagger.v3.oas.annotations.Operation;
//import lombok.RequiredArgsConstructor;
//import org.springframework.web.bind.annotation.*;
//import org.springframework.web.multipart.MultipartFile;
//
//import java.io.IOException;
//
//@RestController
//@RequiredArgsConstructor
//@RequestMapping("/v1/temp")
//public class TempController {
//    private final TempService tempService;
//
//    /**
//     *
//     * @param csvFile 가게 정보 파일
//     * @return 데이터 베이스 업로드
//     * @throws IOException -
//     */
//    @PostMapping("/store/file-upload")
//    @Operation(summary = "가게 정보 대량 등록")
//    public CommonResult setStoreByFile(@RequestParam("csvFile") MultipartFile csvFile) throws IOException {
//        tempService.setStoreByFile(csvFile);
//
//        return ResponseService.getSuccessResult();
//    }
//    /**
//     *
//     * @param csvFile 라이더 정보 파일
//     * @return 데이터 베이스 업로드
//     * @throws IOException -
//     */
//
//    @PostMapping("rider/file-upload")
//    @Operation(summary = "라이더 정보 대량 등록")
//    public CommonResult setRiderByFile(@RequestParam("csvFile") MultipartFile csvFile) throws IOException {
//        tempService.setRiderByFile(csvFile);
//
//        return ResponseService.getSuccessResult();
//    }
//
//    /**
//     *
//     * @return 최근 10일간의 회원 가입자 수를 구한다.
//     */
//    @GetMapping("/member-join-day-list")
//    @Operation(summary = "최근 10일간의 회원 가입자 수를 구한다.")
//    public SingleResult<NearDayStatisticsResponse> getNearDayStatistics() {
//        return ResponseService.getSingleResult(tempService.getNearDayStatistics());
//    }
//
//    @PostMapping("/upload")
//    public CommonResult uploadImage(@RequestParam(name = "file")MultipartFile file) throws IOException {
//        tempService.uploadImage(file);
//        return ResponseService.getSuccessResult();
//    }
//}
