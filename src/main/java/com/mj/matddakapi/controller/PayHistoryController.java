//package com.mj.matddakapi.controller;
//
//import com.mj.matddakapi.entity.Rider;
//import com.mj.matddakapi.entity.RiderPay;
//import com.mj.matddakapi.model.generic.CommonResult;
//import com.mj.matddakapi.model.payhistory.PayHistoryCreateRequest;
//import com.mj.matddakapi.service.*;
//import io.swagger.v3.oas.annotations.Operation;
//import jakarta.validation.Valid;
//import lombok.RequiredArgsConstructor;
//import org.springframework.web.bind.annotation.*;
//
//@RestController
//@RequiredArgsConstructor
//@RequestMapping("/v1/pay-history")
//public class PayHistoryController {
//    private final PayHistoryService payHistoryService;
//    private final RiderService riderService;
//    private final ProfileService profileService;
//
//    @PostMapping("/in")
//    @Operation(summary = "페이 증가")
//    public CommonResult setPayIn(@RequestBody @Valid PayHistoryCreateRequest request) {
//        Rider rider = profileService.getData();
//        payHistoryService.setPayIn(rider, request);
//
//        return ResponseService.getSuccessResult();
//    }
//
//    @PostMapping("/out")
//    @Operation(summary = "페이 감소")
//    public CommonResult setPayOut(@RequestBody @Valid PayHistoryCreateRequest request) throws Exception {
//        Rider rider = profileService.getData();
//        payHistoryService.setPayOut(rider, request);
//
//        return ResponseService.getSuccessResult();
//    }
//}
