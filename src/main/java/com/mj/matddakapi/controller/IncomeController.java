package com.mj.matddakapi.controller;

import com.mj.matddakapi.entity.Income;
import com.mj.matddakapi.entity.Rider;
import com.mj.matddakapi.model.generic.ListResult;
import com.mj.matddakapi.model.generic.SingleResult;
import com.mj.matddakapi.model.income.IncomeItem;
import com.mj.matddakapi.model.income.IncomeResponse;
import com.mj.matddakapi.model.statistics.IncomeStatisticsAdminResponse;
import com.mj.matddakapi.model.statistics.IncomeStatisticsItem;
import com.mj.matddakapi.model.statistics.IncomeStatisticsResponse;
import com.mj.matddakapi.service.IncomeService;
import com.mj.matddakapi.service.ProfileService;
import com.mj.matddakapi.service.ResponseService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;


@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/income")
public class IncomeController {
    private final IncomeService incomeService;
    private final ProfileService profileService;

    @GetMapping("/all/{pageNum}")
    @Operation(summary = "실적 복수 최신순 페이징")
    public ListResult<IncomeItem> getIncomesP(@PathVariable int pageNum) {
        return ResponseService.getListResult(incomeService.getIncomesP(pageNum), true);
    }

    @GetMapping("/detail/{incomeId}")
    @Operation(summary = "실적 단수")
    public SingleResult<IncomeResponse> getIncome(@PathVariable long incomeId) {
        return ResponseService.getSingleResult(incomeService.getIncome(incomeId));
    }

//    @GetMapping("/date/admin")
//    @Operation(summary = "관리자 일간 수익")
//    public SingleResult<IncomeStatisticsResponse> getDayIncomeAdmin() {
//        return ResponseService.getSingleResult(incomeService.getDateIncomeAdmin());
//    }

    @GetMapping("/date/rider")
    @Operation(summary = "라이더 일간 수익 단수 TOKEN")
    public SingleResult<IncomeStatisticsResponse> getDateIncomeRider() {
        Rider rider = profileService.getData();
        return ResponseService.getSingleResult(incomeService.getDateIncomeRider(rider));
    }

//    @GetMapping("/week/admin")
//    @Operation(summary = "관리자 주간 수익")
//    public SingleResult<IncomeStatisticsResponse> getWeekIncomeAdmin() {
//        return ResponseService.getSingleResult(incomeService.getWeekIncomeAdmin());
//    }

    @GetMapping("/week/rider")
    @Operation(summary = "라이더 주간 수익 단수 TOKEN")
    public SingleResult<IncomeStatisticsResponse> getWeekIncomeRider() {
        Rider rider = profileService.getData();
        return ResponseService.getSingleResult(incomeService.getWeekIncomeRider(rider));
    }

//    @GetMapping("/month/admin")
//    @Operation(summary = "관리자 월간 수익")
//    public SingleResult<IncomeStatisticsResponse> getMonthIncomeAdmin() {
//        return ResponseService.getSingleResult(incomeService.getMonthIncomeAdmin());
//    }

    @GetMapping("/month/rider")
    @Operation(summary = "라이더 월간 수익 단수 TOKEN")
    public SingleResult<IncomeStatisticsResponse> getMonthIncomeRider() {
        Rider rider = profileService.getData();
        return ResponseService.getSingleResult(incomeService.getMonthIncomeRider(rider));
    }

    @GetMapping("/all/week/rider")
    @Operation(summary = "라이더 주간 수익 하루 단위 복수 TOKEN")
    public ListResult<IncomeStatisticsItem> getWeekIncomesRider() {
        Rider rider = profileService.getData();
        return ResponseService.getListResult(incomeService.getWeekIncomesRider(rider),true);
    }

    @GetMapping("/all/month/rider")
    @Operation(summary = "라이더 월간 수익 하루 단위 복수 TOKEN")
    public ListResult<IncomeStatisticsItem> getMonthIncomesRider() {
        Rider rider = profileService.getData();
        return ResponseService.getListResult(incomeService.getMonthIncomesRider(rider),true);
    }

    @GetMapping("/all/term/rider")
    @Operation(summary = "라이더 수익 검색 하루 단위 복수 TOKEN(기간 검색)")
    public ListResult<IncomeStatisticsItem> getIncomesRiderSearch(@RequestParam(name = "startDay")String startDay, @RequestParam(name = "endDay")String endDay)  {
        Rider rider = profileService.getData();
        return ResponseService.getListResult(incomeService.getIncomesRiderSearch(startDay, endDay, rider), true);
    }

    @GetMapping("/statistics/admin")
    @Operation(summary = "관리자 수익 통계 단수 (일간, 주간, 월간)")
    public SingleResult<IncomeStatisticsAdminResponse> getIncomeStatisticsAdmin() {
        return ResponseService.getSingleResult(incomeService.getIncomeStatisticsAdmin());
    }
}
