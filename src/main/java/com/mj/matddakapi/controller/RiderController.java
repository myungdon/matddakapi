package com.mj.matddakapi.controller;

import com.mj.matddakapi.entity.Rider;
import com.mj.matddakapi.enums.rider.Admin;
import com.mj.matddakapi.model.generic.CommonResult;
import com.mj.matddakapi.model.generic.ListResult;
import com.mj.matddakapi.model.generic.SingleResult;
import com.mj.matddakapi.model.rider.*;
import com.mj.matddakapi.model.rider.person.RiderImgChangeRequest;
import com.mj.matddakapi.model.rider.person.RiderPersonChangeRequest;
import com.mj.matddakapi.model.rider.person.RiderPhoneTypeChangeRequest;
import com.mj.matddakapi.model.rider.person.RiderTypeChangeRequest;
import com.mj.matddakapi.service.ProfileService;
import com.mj.matddakapi.service.ResponseService;
import com.mj.matddakapi.service.RiderService;
import io.swagger.v3.oas.annotations.Operation;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequiredArgsConstructor
@RequestMapping("v1/rider")
public class RiderController {
    private final RiderService riderService;
    private final ProfileService profileService;

    @PostMapping(value = "/join/admin") // 회원가입
    @Operation(summary = "관리자 회원가입")
    public CommonResult setRiderAdmin(@RequestBody RiderJoinRequest request) {
        riderService.setRider(Admin.ROLE_ADMIN, request);

        return ResponseService.getSuccessResult();
    }

    @PostMapping(value = "/join/normal", consumes = MediaType.MULTIPART_FORM_DATA_VALUE) // 회원가입
    @Operation(summary = "일반 회원가입")
    public CommonResult setRiderNormal(RiderJoinRequest request, @RequestParam(name = "parentsFile", required = false)MultipartFile parentsFile) throws IOException {
        if (parentsFile == null || parentsFile.isEmpty()) {
            riderService.setRider(Admin.ROLE_NORMAL, request);
        } else {
            riderService.setRider(Admin.ROLE_NORMAL, request, parentsFile);
        }

        return ResponseService.getSuccessResult();
    }
    @GetMapping("/all") //회원 리스트
    @Operation(summary = "회원 리스트 (관리자)")
    public ListResult<RiderItem> getRiders() {return ResponseService.getListResult(riderService.getRiders(), true);}

    @GetMapping("/detail") // 개인 회원 상세 보기
    @Operation(summary = "개인 회원 상세보기 (라이더) TOKEN")
    public SingleResult<RiderResponse> getRider() {
        Rider rider = profileService.getData();
        return ResponseService.getSingleResult(riderService.getRider(rider));
    }

    @GetMapping("detail/{id}") // 관리자 : 개인회원 상세 보기
    @Operation(summary = "개인 회원 상세 보기 (관리자)")
    public SingleResult<RiderResponse> getAdminRider(@PathVariable long id){
        return ResponseService.getSingleResult(riderService.getAdminRider(id));
    }

    @GetMapping("/all/{pageNum}") //라이더 페이징
    @Operation(summary = "라이더 리스트 페이징")
    public ListResult<RiderItem> getPages(@PathVariable int pageNum){
        return riderService.getPages(pageNum);
    }

    @PutMapping("/change/{id}") // 라이더 정보 수정
    @Operation(summary = "회원 정보 수정 (관리자)")
    public CommonResult putRiders(@PathVariable long id, @RequestBody @Valid RiderChangeRequest request){
        riderService.putRiders(id, request);

        return ResponseService.getSuccessResult();
    }

    @PutMapping(value = "/change") // 라이더 개인정보 수정
    @Operation(summary = "개인 정보 수정 (라이더) TOKEN")
    public CommonResult putRider(@RequestBody RiderPersonChangeRequest request) {
        Rider rider = profileService.getData();
        riderService.putRider(rider, request);

        return ResponseService.getSuccessResult();
    }

//        @PutMapping("/bank/{id}")
//    @Operation(summary = "라이더 계좌 수정 (라이더)")
//    public CommonResult putBank(@PathVariable long id, @RequestBody RiderBankChangeRequest request){
//        riderService.putBank(id, request);
//
//        return ResponseService.getSuccessResult();
//    }
//    @PutMapping("/type")
//    @Operation(summary = "라이더 운송수단 변경 (라이더) TOKEN")
//    public CommonResult putRidingType(@RequestBody @Valid RiderTypeChangeRequest request){
//        Rider rider = profileService.getData();
//        riderService.putRidingType(rider, request);
//
//        return ResponseService.getSuccessResult();
//    }

    @GetMapping("/black-list/all")
    @Operation(summary = "라이더 블랙리스트")
    public ListResult<RiderItem> getBlackList(){
        return ResponseService.getListResult(riderService.getBlackList(), true);
    }

//    @PutMapping("/phone-type")
//    @Operation(summary = "라이더 기종 변경 (라이더) TOKEN")
//    public CommonResult putPhoneType(@RequestBody @Valid RiderPhoneTypeChangeRequest request){
//        Rider rider = profileService.getData();
//        riderService.putPhoneType(rider, request);
//
//        return ResponseService.getSuccessResult();
//    }

    @PutMapping(value = "/change/img", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @Operation(summary = "라이더 프사 변경 (라이더) TOKEN")
    public CommonResult putRiderImg(@RequestParam(name = "file", required = false) MultipartFile file) throws IOException {
        Rider rider = profileService.getData();
        riderService.putRiderImg(rider, file);

        return ResponseService.getSuccessResult();
    }
}
