package com.mj.matddakapi.controller;

import com.mj.matddakapi.entity.Rider;
import com.mj.matddakapi.model.generic.CommonResult;
import com.mj.matddakapi.model.generic.ListResult;
import com.mj.matddakapi.model.generic.SingleResult;
import com.mj.matddakapi.model.moneyhistory.MoneyHistoryCreateRequest;
import com.mj.matddakapi.model.moneyhistory.MoneyHistoryItem;
import com.mj.matddakapi.model.moneyhistory.MoneyHistoryResponse;
import com.mj.matddakapi.service.MoneyHistoryService;
import com.mj.matddakapi.service.ProfileService;
import com.mj.matddakapi.service.ResponseService;
import com.mj.matddakapi.service.RiderService;
import io.swagger.v3.oas.annotations.Operation;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/money-history")
public class MoneyHistoryController {
    private final MoneyHistoryService moneyHistoryService;
    private final RiderService riderService;
    private final ProfileService profileService;

    @PostMapping("/in")
    @Operation(summary = "입금 신청 TOKEN")
    public CommonResult setMoneyIn(@RequestBody @Valid MoneyHistoryCreateRequest request) {
        Rider rider = profileService.getData();
        moneyHistoryService.setMoneyIn(rider, request);

        return ResponseService.getSuccessResult();
    }

    @PostMapping("/out")
    @Operation(summary = "출금 신청 TOKEN")
    public CommonResult setMoneyOut(@RequestBody @Valid MoneyHistoryCreateRequest request) throws Exception {
        Rider rider = profileService.getData();
        moneyHistoryService.setMoneyOut(rider, request);

        return ResponseService.getSuccessResult();
    }

    @GetMapping("/all/out")
    @Operation(summary = "복수 출금만 라이더 1명 최신순 TOKEN")
    public ListResult<MoneyHistoryItem> getMoneyOuts(){
        Rider rider = profileService.getData();
        return ResponseService.getListResult(moneyHistoryService.getMoneyOuts(rider), true);
    }

    @GetMapping("/detail/{moneyId}")
    @Operation(summary = "입출금 단수")
    public SingleResult<MoneyHistoryResponse> getMoneyHistory(@PathVariable long moneyId) {
        return ResponseService.getSingleResult(moneyHistoryService.getMoneyHistory(moneyId));
    }

    @GetMapping("/all/out/{pageNum}")
    @Operation(summary = "복수 출금만 최신순 페이징")
    public ListResult<MoneyHistoryItem> getMoneyOutsP(@PathVariable int pageNum) {
        return ResponseService.getListResult(moneyHistoryService.getMoneyOutsP(pageNum), true);
    }
}
