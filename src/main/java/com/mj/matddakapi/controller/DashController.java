package com.mj.matddakapi.controller;

import com.mj.matddakapi.model.dash.DashResponse;
import com.mj.matddakapi.model.generic.SingleResult;
import com.mj.matddakapi.service.DashService;
import com.mj.matddakapi.service.ResponseService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/dash")
public class DashController {
    private final DashService dashService;

    @GetMapping("/board")
    @Operation(summary = "웹 대쉬보드 (최근 가입자수, 최근 가입자 8명, 최근 공지사항 8개)")
    public SingleResult<DashResponse> getDash(){
        return ResponseService.getSingleResult(dashService.getDash());
    }
}
