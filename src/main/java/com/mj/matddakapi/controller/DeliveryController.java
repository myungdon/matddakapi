package com.mj.matddakapi.controller;

import com.mj.matddakapi.entity.Ask;
import com.mj.matddakapi.entity.Rider;
import com.mj.matddakapi.model.delivery.DeliveryItem;
import com.mj.matddakapi.model.delivery.DeliveryResponse;
import com.mj.matddakapi.model.delivery.TabCountResponse;
import com.mj.matddakapi.model.generic.CommonResult;
import com.mj.matddakapi.model.generic.ListResult;
import com.mj.matddakapi.model.generic.SingleResult;
import com.mj.matddakapi.service.*;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/delivery")
public class DeliveryController {
    private final DeliveryService deliveryService;
    private final RiderService riderService;
    private final AskService askService;
    private final ProfileService profileService;

    @PostMapping("/new/ask-id/{askId}")
    @Operation(summary = "배차 신청 TOKEN")
    public CommonResult setDelivery(@PathVariable long askId) {
        Rider rider = profileService.getData();
        Ask ask = askService.getData(askId);
        deliveryService.setDelivery(rider, ask);

        return ResponseService.getSuccessResult();
    }

    @PutMapping("/change/go")
    @Operation(summary = "출발 상태로 변경 TOKEN")
    public CommonResult putGo() {
        Rider rider = profileService.getData();
        deliveryService.putGo(rider);

        return ResponseService.getSuccessResult();
    }

    @PutMapping("/change/done")
    @Operation(summary = "완료 상태로 변경 TOKEN")
    public CommonResult putDone() {
        Rider rider = profileService.getData();
        deliveryService.putDone(rider);

        return ResponseService.getSuccessResult();
    }

    @PutMapping("/change/cancel")
    @Operation(summary = "요청 상태로 변경 TOKEN")
    public CommonResult putCancel() {
        Rider rider = profileService.getData();
        deliveryService.putCancel(rider);

        return ResponseService.getSuccessResult();
    }

    @GetMapping("/all")
    @Operation(summary = "배송 복수 최신순")
    public ListResult<DeliveryItem> getDeliveries() {
        return ResponseService.getListResult(deliveryService.getDeliveries(), true);
    }

    @GetMapping("/all/{pageNum}")
    @Operation(summary = "배송 복수 최신순 페이징")
    public ListResult<DeliveryItem> getDeliveriesP(@PathVariable int pageNum) {
        return ResponseService.getListResult(deliveryService.getDeliveriesP(pageNum), true);
    }

    @GetMapping("/all/pick")
    @Operation(summary = "배송 복수 배차만 최신순")
    public ListResult<DeliveryItem> getPick() {
        return ResponseService.getListResult(deliveryService.getPick(), true);
    }

    @GetMapping("/all/pick/{pageNum}")
    @Operation(summary = "배송 복수 배차만 최신순 페이징")
    public ListResult<DeliveryItem> getPickP(@PathVariable int pageNum) {
        return ResponseService.getListResult(deliveryService.getPickP(pageNum), true);
    }

    @GetMapping("/all/go")
    @Operation(summary = "배송 복수 출발만 최신순")
    public ListResult<DeliveryItem> getGo() {
        return ResponseService.getListResult(deliveryService.getGo(), true);
    }

    @GetMapping("/all/go/{pageNum}")
    @Operation(summary = "배송 복수 출발만 최신순 페이징")
    public ListResult<DeliveryItem> getGoP(@PathVariable int pageNum) {
        return ResponseService.getListResult(deliveryService.getGoP(pageNum), true);
    }

    @GetMapping("/all/done")
    @Operation(summary = "배송 복수 완료만 최신순")
    public ListResult<DeliveryItem> getDone() {
        return ResponseService.getListResult(deliveryService.getDone(), true);
    }

    @GetMapping("/all/done/app")
    @Operation(summary = "배송 복수 완료만 최신순 하루치 (라이더) TOKEN")
    public ListResult<DeliveryItem> getDoneApp() {
        Rider rider = profileService.getData();
        return ResponseService.getListResult(deliveryService.getDoneApp(rider), true);
    }

    @GetMapping("/all/done/{pageNum}")
    @Operation(summary = "배송 복수 완료만 최신순 페이징")
    public ListResult<DeliveryItem> getDoneP(@PathVariable int pageNum) {
        return ResponseService.getListResult(deliveryService.getDoneP(pageNum), true);
    }
//    @GetMapping("/all/cancel")
//    @Operation(summary = "배송 복수 취소만 최신순")
//    public ListResult<DeliveryItem> getCancel() {
//        return ResponseService.getListResult(deliveryService.getCancel(), true);
//    }
//
//    @GetMapping("/all/cancel/{pageNum}")
//    @Operation(summary = "배송 복수 취소만 최신순 페이징")
//    public ListResult<DeliveryItem> getCancelP(@PathVariable int pageNum) {
//        return ResponseService.getListResult(deliveryService.getCancelP(pageNum), true);
//    }

    @GetMapping("/detail/{deliveryId}")
    @Operation(summary = "배송 단수 (관리자용)")
    public SingleResult<DeliveryResponse> getDelivery(@PathVariable long deliveryId) {
        return ResponseService.getSingleResult(deliveryService.getDelivery(deliveryId));
    }

    @GetMapping("/detail/pick")
    @Operation(summary = "배송 배차용 단수 (라이더용) TOKEN")
    public SingleResult<DeliveryResponse> getDeliveryPick() {
        Rider rider = profileService.getData();
        return ResponseService.getSingleResult(deliveryService.getDeliveryPick(rider));
    }

    @GetMapping("/detail/go")
    @Operation(summary = "배송 출발용 단수 (라이더용) TOKEN")
    public SingleResult<DeliveryResponse> getDeliveryGo() {
        Rider rider = profileService.getData();
        return ResponseService.getSingleResult(deliveryService.getDeliveryGo(rider));
    }

    @GetMapping("/detail/done/{deliveryId}")
    @Operation(summary = "배송 완료용 단수 (라이더용) TOKEN")
    public SingleResult<DeliveryResponse> getDeliveryDone(@PathVariable long deliveryId) {
        Rider rider = profileService.getData();
        return ResponseService.getSingleResult(deliveryService.getDeliveryDone(deliveryId, rider));
    }

    @GetMapping("/tab")
    @Operation(summary = "앱 탭바 TOKEN")
    public SingleResult<TabCountResponse> getTabCount() {
        Rider rider = profileService.getData();
        return ResponseService.getSingleResult(deliveryService.getTabCount(rider));
    }
}
