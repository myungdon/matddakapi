package com.mj.matddakapi.controller;

import com.mj.matddakapi.entity.Rider;
import com.mj.matddakapi.model.generic.SingleResult;
import com.mj.matddakapi.model.riderinfo.RiderInfoResponse;
import com.mj.matddakapi.service.ProfileService;
import com.mj.matddakapi.service.ResponseService;
import com.mj.matddakapi.service.RiderInfoService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/rider-info")
public class RiderInfoController {
    private final RiderInfoService riderInfoService;
    private final ProfileService profileService;

    @GetMapping("/detail")
    @Operation(summary = "라이더 정보 단수 (앱 drawer) TOKEN")
    public SingleResult<RiderInfoResponse> getRiderInfo() {
        Rider rider = profileService.getData();
        return ResponseService.getSingleResult(riderInfoService.getRiderInfo(rider));
    }
}
