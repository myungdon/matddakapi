package com.mj.matddakapi.controller;

import com.mj.matddakapi.model.board.BoardChangeRequest;
import com.mj.matddakapi.model.board.BoardCreateRequest;
import com.mj.matddakapi.model.board.BoardItem;
import com.mj.matddakapi.model.board.BoardResponse;
import com.mj.matddakapi.model.generic.CommonResult;
import com.mj.matddakapi.model.generic.ListResult;
import com.mj.matddakapi.model.generic.SingleResult;
import com.mj.matddakapi.service.BoardService;
import com.mj.matddakapi.service.ResponseService;
import io.swagger.v3.oas.annotations.Operation;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/board")
public class BoardController {
    private final BoardService boardService;

    @PostMapping(value = "/new", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @Operation(summary = "게시글 등록")
    public CommonResult setBoard(BoardCreateRequest request, @RequestParam(value = "file", required = false)MultipartFile file) throws IOException {
        if (file == null || file.isEmpty()) {
            boardService.setBoard(request);
        } else {
            boardService.setBoard(request, file);
        }

        return ResponseService.getSuccessResult();
    }

    @GetMapping("/all")
    @Operation(summary = "게시글 복수 최신순")
    public ListResult<BoardItem> getBoards() {
        return ResponseService.getListResult(boardService.getBoards(), true);
    }

    @GetMapping("/all/{pageNum}")
    @Operation(summary = "게시글 복수 최신순 페이징")
    public ListResult<BoardItem> getBoardsP(@PathVariable int pageNum) {
        return ResponseService.getListResult(boardService.getBoardsP(pageNum), true);
    }

    @GetMapping("/detail/{boardId}")
    @Operation(summary = "게시글 단수")
    public SingleResult<BoardResponse> getBoard(@PathVariable long boardId) {
        return ResponseService.getSingleResult(boardService.getBoard(boardId));
    }

    @PutMapping(value = "/change/{boardId}", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @Operation(summary = "게시글 수정")
    public CommonResult putBoard(@PathVariable long boardId, BoardChangeRequest request, @RequestParam(value = "file", required = false)MultipartFile file) throws IOException {
        if (file == null || file.isEmpty()) {
            boardService.putBoard(boardId, request);
        } else {
            boardService.putBoard(boardId, request, file);
        }


        return ResponseService.getSuccessResult();
    }

    @DeleteMapping("/delete/{boardId}")
    @Operation(summary = "게시글 삭제")
    public CommonResult delBoard(@PathVariable long boardId) {
        boardService.delBoard(boardId);

        return ResponseService.getSuccessResult();
    }
}
