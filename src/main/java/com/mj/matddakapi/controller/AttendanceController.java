package com.mj.matddakapi.controller;

import com.mj.matddakapi.entity.Rider;
import com.mj.matddakapi.model.attendance.AttendanceResponse;
import com.mj.matddakapi.model.generic.CommonResult;
import com.mj.matddakapi.model.generic.SingleResult;
import com.mj.matddakapi.service.AttendanceService;
import com.mj.matddakapi.service.ProfileService;
import com.mj.matddakapi.service.ResponseService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/attendance")
public class AttendanceController {
    private final AttendanceService attendanceService;
    private final ProfileService profileService;

    @GetMapping("/detail")
    @Operation(summary = "근태 단수 라이더용 TOKEN")
    public SingleResult<AttendanceResponse> getAttendance() {
        Rider rider = profileService.getData();
        return ResponseService.getSingleResult(attendanceService.getAttendance(rider));
    }

    @PutMapping("/change/{attendanceId}")
    @Operation(summary = "근무 상태 변경")
    public CommonResult putAttendance(@PathVariable long attendanceId) {
        attendanceService.putAttendance(attendanceId);
        return ResponseService.getSuccessResult();
    }
}
