package com.mj.matddakapi.enums.payHistory;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum PayType {
    IN("입금")
    ,OUT("출금");

    private final String name;
}
