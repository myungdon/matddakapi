package com.mj.matddakapi.enums.ask;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum StateAsk {
    REQUESTING("요청")
    ,S_PICK("요청완료")
    ,S_CANCEL("주문취소")
    ,S_DONE("배달완료");

    private final String name;
}
