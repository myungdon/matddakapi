package com.mj.matddakapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ResultCode {
    SUCCESS(0,"성공하였습니다.")
    ,FAILED(-1,"실패하였습니다.")

    ,JOIN_NULL(-1000, "필수 값을 미입력하였습니다.") // O
    ,EMAIL_SAME(-1001, "중복된 이메일입니다.") // O
    ,EMAIL_FORM(-1002, "올바른 이메일 형식이 아닙니다.") // O
    ,AGE_UNDER(-1006, "15미만은 가입할 수 없습니다.") // o
    ,PARENT_SUBMIT(-1007, "부모님 동의서를 제출해 주시기 바랍니다.") // 파일 업로드 후에 하기
    ,FORMAT_DIFF(-1008, "지원 하지 않는 파일 형식입니다.") // 파일 업로드 후에 하기
    ,ID_NUM_LENGTH(-1009, "주민등록번호를 확인해 주시기 바랍니다.")// o
    ,PHONE_LENGTH(-1010, "핸드폰 번호를 확인해 주시기 바랍니다.")// o
    ,PHONE_NULL(-1011, "기기를 등록해 주시기 바랍니다.") // 보류

    ,LOAD_DELAY(-2000, "정보를 불러 오는데 실패 하였습니다.")

    ,DELIVERY_PICK(-3000, "이미 배차된 주문입니다.") // o
    ,DELIVERY_DONE(-3001, "이미 완료된 배송입니다.")
    ,DELIVERY_ONLY_ONE(-3002, "배차는 1개 밖에 선택 할 수 없습니다.") // o

//    ,RIDER_INFO_SAME(-4000, "변경된 회원정보 값이 없습니다.")

    ,RIDER_PAY_TERM_OVER(-5000, "조회 가능한 기간을 초과하였습니다. 최근 60일 이내만 조회 가능 합니다.")// o

    ,MONEY_OUT_OVER(-6000, "출금 가능한 금액을 초과하였습니다.") // o

    ,RIDER_MISSING(-7000, "회원정보를 찾을 수 없습니다.")
    ,PASSWORD_DIFFERENT(-7001, "비밀번호를 확인 해 주십시요.")

    ,MISSING_DATA(-10000, "데이터를 찾을 수 없습니다.");

    private final Integer code;
    private final String msg;
}
