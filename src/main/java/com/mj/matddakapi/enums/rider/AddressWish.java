package com.mj.matddakapi.enums.rider;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum AddressWish {
    DANWON("안산시 단원구")
    ,SANGROCK("안산시 상록구");

    private final String name;
}
