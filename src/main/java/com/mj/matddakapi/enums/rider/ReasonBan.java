package com.mj.matddakapi.enums.rider;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@AllArgsConstructor
public enum ReasonBan {
    PARENTAL_CONSENT("부모님 동의서 미제출"),
    SUSPENSION_OF_LICENSE("면허 정지"),
    NOT_BAN("해당없음");
    

    private final String name;
}
