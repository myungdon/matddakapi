package com.mj.matddakapi.enums.rider;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum BankName {
    KUKMIN("국민")
    ,HANA("하나")
    ,SINHAN("신한")
    ,URI("우리")
    ,SCJEIL("SC제일")
    ,HANKUKCITY("한국씨티")
    ,KBANK("케이뱅크")
    ,KAKAO("카카오뱅크")
    ,TOS("토스뱅크")
    ,ETC("기타");

    private final String name;
}
