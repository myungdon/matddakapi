package com.mj.matddakapi.enums.rider;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum PhoneType {
    INIT_VALUE("초기값")
    ,SMART_PHONE("스마트폰")
    ,PDA("PDA");

    private final String name;
}
