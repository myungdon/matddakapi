package com.mj.matddakapi.lib;

public class CommonCheck {
    /**
     * 정규식 왜 써야 할까?
     * 사람들이 입력하는 아이디 "킹왕짱면돈", "404" 이런거 DB에 등록해주면 안되니까
     * 그런데 이거 안된다~~ 라는걸. 아이디는 string이니까 if문으로 처리 할 수 있나?
     * 그래서 정규식 한다
     */
    public static boolean checkEmail(String email) {
        String pattern = "^[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*.[a-zA-Z]{2,3}$";
        return email.matches(pattern);
    }
    public static boolean checkIdNum(String idNum) {
        String pattern = "\\d{2}([0]\\d|[1][0-2])([0][1-9]|[1-2]\\d|[3][0-1])[-]*[1-4]\\d{6}";
        return idNum.matches(pattern);
    }

    public static boolean checkPhoneNumber(String phoneNumber) {
        String pattern = "^[\\d]{2,3}-[\\d]{3,4}-[\\d]{4}+$";
        return phoneNumber.matches(pattern);
    }
}
