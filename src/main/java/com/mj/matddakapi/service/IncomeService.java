package com.mj.matddakapi.service;

import com.mj.matddakapi.entity.Ask;
import com.mj.matddakapi.entity.Income;
import com.mj.matddakapi.entity.Rider;
import com.mj.matddakapi.enums.ask.StateAsk;
import com.mj.matddakapi.exception.CRiderPayTermOverException;
import com.mj.matddakapi.model.generic.ListResult;
import com.mj.matddakapi.model.income.IncomeItem;
import com.mj.matddakapi.model.income.IncomeResponse;
import com.mj.matddakapi.model.statistics.IncomeStatisticsAdminResponse;
import com.mj.matddakapi.model.statistics.IncomeStatisticsItem;
import com.mj.matddakapi.model.statistics.IncomeStatisticsResponse;
import com.mj.matddakapi.repository.AskRepository;
import com.mj.matddakapi.repository.IncomeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Service
@RequiredArgsConstructor
public class IncomeService {
    private final IncomeRepository incomeRepository;
    private final AskRepository askRepository;

    /**
     * 실적 복수 최신순 페이징
     */
    public ListResult<IncomeItem> getIncomesP(int pageNum) {
        PageRequest pageRequest = ListConvertService.getPageable(pageNum);
        Page<Income> incomes = incomeRepository.findAllByOrderByIdDesc(pageRequest);

        List<IncomeItem> response = new LinkedList<>();
        for (Income income : incomes) response.add(new IncomeItem.Builder(income).build());

        return ListConvertService.settingResult(response, incomes.getTotalElements(), incomes.getTotalPages(), incomes.getPageable().getPageNumber());
    }

    /**
     * 실적 단수
     */
    public IncomeResponse getIncome(long incomeId) {
        Income originData = incomeRepository.findById(incomeId).orElseThrow();

        return new IncomeResponse.Builder(originData).build();
    }

    /**
     * 관리자 일간 수익 단수
     */
    public IncomeStatisticsResponse getDateIncomeAdmin() {
        LocalDate today = LocalDate.now();
        List<Income> originList = incomeRepository.findAllByDateIncome(today);
        IncomeStatisticsResponse response = new IncomeStatisticsResponse();

        double totalDay = 0D;
        for (Income income : originList){
            totalDay += income.getFeeAdmin();
        }

        response.setDateTotalIncome(today.toString());
        response.setTotalAdmin(totalDay);

        return response;
    }

    /**
     * 라이더 일간 수익 단수
     */
    public IncomeStatisticsResponse getDateIncomeRider(Rider rider) {
        LocalDate today = LocalDate.now();
        List<Income> originList = incomeRepository.findAllByDateIncomeAndDelivery_Rider_Id(today, rider.getId());
        List<Ask> originList2 = askRepository.findAllByDateAskAndStateAsk(today, StateAsk.S_CANCEL);

        IncomeStatisticsResponse response = new IncomeStatisticsResponse();

        int totalCount = originList.size();
        int totalCancelCount = originList2.size();
        double totalFeeDay = 0D;
        double totalFeeAdmin = 0D;
        double totalFeeRider = 0D;
        double totalDistance = 0D;
        for (Income income : originList){
            totalFeeDay += income.getFeeTotal();
            totalFeeAdmin += income.getFeeAdmin();
            totalFeeRider += income.getFeeRider();
            totalDistance += (double) Math.round(income.getDelivery().getAsk().getDistance() * 1000) / 1000;
        }
        double perDistance = (double) Math.round(totalDistance / totalCount * 1000) / 1000;

        response.setDateTotalIncome(today.toString());
        response.setTotalCount(totalCount);
        response.setTotalFee(totalFeeDay);
        response.setTotalAdmin(totalFeeAdmin);
        response.setTotalRider(totalFeeRider);
        response.setTotalDistance(totalDistance);
        response.setPerDistance(perDistance);
        response.setTotalCancelCount(totalCancelCount);

        return response;
    }

    /**
     * 관리자 주간 수익 단수
     */
    public IncomeStatisticsResponse getWeekIncomeAdmin() {
        LocalDate today = LocalDate.now();
        LocalDate startDay = today.minusDays(6);
        List<Income> originList = incomeRepository.findAllByDateIncomeBetween(startDay, today);

        IncomeStatisticsResponse response = new IncomeStatisticsResponse();

        double totalWeek = 0D;
        for (Income income : originList){
            totalWeek += income.getFeeAdmin();
        }

        response.setDateTotalIncome(startDay + " ~ " + today);
        response.setTotalAdmin(totalWeek);

        return response;
    }

    /**
     * 라이더 주간 수익 단수
     */
    public IncomeStatisticsResponse getWeekIncomeRider(Rider rider) {
        LocalDate today = LocalDate.now();
        LocalDate startDay = today.minusDays(6);
        List<Income> originList = incomeRepository.findAllByDateIncomeBetweenAndDelivery_Rider_Id(startDay, today, rider.getId());
        List<Ask> originList2 = askRepository.findAllByDateAskAndStateAsk(today, StateAsk.S_CANCEL);

        IncomeStatisticsResponse response = new IncomeStatisticsResponse();

        int totalCount = originList.size();
        int totalCancelCount = originList2.size();
        double totalFeeWeek = 0D;
        double totalFeeAdmin = 0D;
        double totalFeeRider = 0D;
        double totalDistance = 0D;
        for (Income income : originList){
            totalFeeWeek += income.getFeeTotal();
            totalFeeAdmin += income.getFeeAdmin();
            totalFeeRider += income.getFeeRider();
            totalDistance += (double) Math.round(income.getDelivery().getAsk().getDistance() * 1000) / 1000;
        }
        double perDistance = (double) Math.round(totalDistance / totalCount * 1000) / 1000;

        response.setDateTotalIncome(startDay + " ~ " + today);
        response.setTotalCount(totalCount);
        response.setTotalFee(totalFeeWeek);
        response.setTotalAdmin(totalFeeAdmin);
        response.setTotalRider(totalFeeRider);
        response.setTotalDistance(totalDistance);
        response.setPerDistance(perDistance);
        response.setTotalCancelCount(totalCancelCount);

        return response;
    }

    /**
     * 관리자 월간 수익 단수
     */
    public IncomeStatisticsResponse getMonthIncomeAdmin() {
        LocalDate today = LocalDate.now();
        LocalDate startDay = today.minusDays(29);
        List<Income> originList = incomeRepository.findAllByDateIncomeBetween(startDay, today);

        IncomeStatisticsResponse response = new IncomeStatisticsResponse();

        double totalMonth = 0D;
        for (Income income : originList){
            totalMonth += income.getFeeAdmin();
        }

        response.setDateTotalIncome(today.toString().substring(0, 7));
        response.setTotalAdmin(totalMonth);

        return response;
    }

    /**
     * 라이더 월간 수익 단수
     */
    public IncomeStatisticsResponse getMonthIncomeRider(Rider rider) {
        LocalDate today = LocalDate.now();
        LocalDate startDay = today.minusDays(29);
        List<Income> originList = incomeRepository.findAllByDateIncomeBetweenAndDelivery_Rider_Id(startDay, today, rider.getId());
        List<Ask> originList2 = askRepository.findAllByDateAskAndStateAsk(today, StateAsk.S_CANCEL);

        IncomeStatisticsResponse response = new IncomeStatisticsResponse();

        int totalCount = originList.size();
        int totalCancelCount = originList2.size();
        double totalFeeMonth = 0D;
        double totalFeeAdmin = 0D;
        double totalFeeRider = 0D;
        double totalDistance = 0D;
        for (Income income : originList){
            totalFeeMonth += income.getFeeTotal();
            totalFeeAdmin += income.getFeeAdmin();
            totalFeeRider += income.getFeeRider();
            totalDistance += (double) Math.round(income.getDelivery().getAsk().getDistance() * 1000) / 1000;
        }
        double perDistance = (double) Math.round(totalDistance / totalCount * 1000) / 1000;

        response.setDateTotalIncome(today.toString().substring(0, 7));
        response.setTotalCount(totalCount);
        response.setTotalFee(totalFeeMonth);
        response.setTotalAdmin(totalFeeAdmin);
        response.setTotalRider(totalFeeRider);
        response.setTotalDistance(totalDistance);
        response.setPerDistance(perDistance);
        response.setTotalCancelCount(totalCancelCount);

        return response;
    }

    /**
     * 라이더 주간 수익 복수
     */
    public ListResult<IncomeStatisticsItem> getWeekIncomesRider(Rider rider) {
        LocalDate today = LocalDate.now();
        LocalDate startDay = today.minusDays(6);
        List<Income> originList = incomeRepository.findAllByDateIncomeBetweenAndDelivery_Rider_Id(startDay, today, rider.getId());
        List<Ask> originList2 = askRepository.findAllByDateAskAndStateAsk(today, StateAsk.S_CANCEL);
        List<IncomeStatisticsItem> result = new LinkedList<>();

        int totalCount = 0;
        int totalCancelCount = 0;
        for (int i = 0; i < 7; i++) {
            IncomeStatisticsItem addItem = new IncomeStatisticsItem();
            LocalDate date = today.minusDays(i);

            double totalFeeRider = 0D;
            double totalDayRider = 0D;
            for (Income income : originList) {
                totalFeeRider += income.getFeeRider();
                if (income.getDateIncome().equals(date)) {
                    totalDayRider += income.getFeeRider();
                    totalCount ++;
                }
            }

            for (Ask ask : originList2) {
                if (ask.getDateAsk().equals(date)) {
                    totalCancelCount ++;
                }
            }

            addItem.setDateTotalIncome(date.toString());
            addItem.setTotalRider(totalFeeRider);
            addItem.setTotalDayRider(totalDayRider);
            addItem.setTotalCount(totalCount);
            addItem.setTotalCancelCount(totalCancelCount);

            result.add(addItem);
        }

        return ListConvertService.settingResult(result);
    }

    /**
     * 라이더 월간 수익 복수
     */
    public ListResult<IncomeStatisticsItem> getMonthIncomesRider(Rider rider) {
        LocalDate today = LocalDate.now();
        LocalDate startDay = today.minusDays(29);
        List<Income> originList = incomeRepository.findAllByDateIncomeBetweenAndDelivery_Rider_Id(startDay, today, rider.getId());
        List<Ask> originList2 = askRepository.findAllByDateAskAndStateAsk(today, StateAsk.S_CANCEL);
        List<IncomeStatisticsItem> result = new LinkedList<>();

        int totalCount = 0;
        int totalCancelCount = 0;
        for (int i = 0; i < 30; i++) {
            IncomeStatisticsItem addItem = new IncomeStatisticsItem();
            LocalDate date = today.minusDays(i);

            double totalFeeRider = 0D;
            double totalDayRider = 0D;
            for (Income income : originList) {
                totalFeeRider += income.getFeeRider();
                if (income.getDateIncome().equals(date)) {
                    totalDayRider += income.getFeeRider();
                    totalCount ++;
                }
            }

            for (Ask ask : originList2) {
                if (ask.getDateAsk().equals(date)) {
                    totalCancelCount ++;
                }
            }

            addItem.setDateTotalIncome(date.toString());
            addItem.setTotalRider(totalFeeRider);
            addItem.setTotalDayRider(totalDayRider);
            addItem.setTotalCount(totalCount);
            addItem.setTotalCancelCount(totalCancelCount);

            result.add(addItem);
        }

        return ListConvertService.settingResult(result);
    }

    /**
     * 라이더 수익 검색 하루 단위 복수
     * @param startDay
     * @param endDay
     * @return
     */
    public ListResult<IncomeStatisticsItem> getIncomesRiderSearch(String startDay, String endDay, Rider rider) {
        // 스트링 타입 날짜를 로컬 데이트로 변경
        LocalDate date1 = LocalDate.parse(startDay, DateTimeFormatter.ISO_DATE);
        LocalDate date2 = LocalDate.parse(endDay, DateTimeFormatter.ISO_DATE);
        // 파라미터 값 넣어주기
        List<Income> originList = incomeRepository.findAllByDateIncomeBetweenAndDelivery_Rider_Id(date1, date2, rider.getId());
        List<IncomeStatisticsItem> result = new LinkedList<>();

        // 로컬데이트를 로컬 데이트 타임으로 변경
        LocalDateTime dateTime1 = date1.atStartOfDay();
        LocalDateTime dateTime2 = date2.atStartOfDay();
        // 시간 차이를 일수로 변경
        int betweenDays = (int) Duration.between(dateTime1, dateTime2).toDays();

        int betweenDays2 = (int) Duration.between(dateTime1, LocalDateTime.now()).toDays();
        if (betweenDays2 > 60) throw new CRiderPayTermOverException();  // 조회 기간 60일로 제한

        // for문으로 일 수만큼 반복
        for (int i = 0; i < betweenDays + 1; i++) {
            IncomeStatisticsItem addItem = new IncomeStatisticsItem();
            // 마지막일부터 하루씩 날짜 차감
            LocalDate date = date2.minusDays(i);

            double totalFeeRider = 0D;
            double totalDayRider = 0D;

            // for문으로 기간 동안의 수익 총합을 구함
            for (Income income : originList) {
                totalFeeRider += Math.round(income.getFeeRider());
                // 날짜가 같은거만 하루동안의 수익 총합을 구함
                if (income.getDateIncome().equals(date)) {
                    totalDayRider += Math.round(income.getFeeRider());
                }
            }

            addItem.setDateTotalIncome(date.toString());
            addItem.setTotalRider(totalFeeRider);
            addItem.setTotalDayRider(totalDayRider);

            result.add(addItem);
        }

        return ListConvertService.settingResult(result);
    }

    /**
     * 관리자 수익 (일간, 주간, 월간)
     */
    public IncomeStatisticsAdminResponse getIncomeStatisticsAdmin() {
        LocalDate today = LocalDate.now();
        LocalDate weekDay = today.minusDays(6);
        LocalDate monthDay = today.minusDays(29);
        List<Income> originList1 = incomeRepository.findAllByDateIncome(today);
        List<Income> originList2 = incomeRepository.findAllByDateIncomeBetween(weekDay, today);
        List<Income> originList3 = incomeRepository.findAllByDateIncomeBetween(monthDay, today);

        IncomeStatisticsAdminResponse response = new IncomeStatisticsAdminResponse();

        int totalDay = 0;
        int totalWeek = 0;
        int totalMonth = 0;
        for (Income income : originList1) {
            totalDay += Math.round(income.getFeeAdmin());
        }

        for (Income income : originList2) {
         totalWeek += Math.round(income.getFeeAdmin());
        }

        for (Income income : originList3) {
            totalMonth += Math.round(income.getFeeAdmin());
        }


        response.setDateDayIncome(today.toString());
        response.setTotalDayAdmin(totalDay);
        response.setDateWeekIncome(weekDay + " ~ " + today);
        response.setTotalWeekAdmin(totalWeek);
        response.setDateMonthIncome(today.toString().substring(0, 7));
        response.setTotalMonthAdmin(totalMonth);

        return response;
    }
}