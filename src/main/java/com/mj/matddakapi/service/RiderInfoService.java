package com.mj.matddakapi.service;

import com.mj.matddakapi.entity.Rider;
import com.mj.matddakapi.entity.RiderInfo;
import com.mj.matddakapi.model.riderinfo.RiderInfoResponse;
import com.mj.matddakapi.repository.RiderInfoRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class RiderInfoService {
    private final RiderInfoRepository riderInfoRepository;

    public RiderInfoResponse getRiderInfo(Rider rider) {
        RiderInfo originData = riderInfoRepository.findById(rider.getId()).orElseThrow();

        return new RiderInfoResponse.Builder(originData).build();
    }
}
