package com.mj.matddakapi.service;

import com.mj.matddakapi.entity.Store;
import com.mj.matddakapi.model.generic.ListResult;
import com.mj.matddakapi.model.store.StoreChangeRequest;
import com.mj.matddakapi.model.store.StoreCreateRequest;
import com.mj.matddakapi.model.store.StoreItem;
import com.mj.matddakapi.model.store.StoreResponse;
import com.mj.matddakapi.repository.StoreRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class StoreService {
    private final StoreRepository storeRepository;

    /**
     * fk용 가게 데이터
     */
    public Store getData(long storeId) {
        return storeRepository.findById(storeId).orElseThrow();
    }

    /**
     * 가게 정보 등록
     */
    public void setStore(StoreCreateRequest request) {
        storeRepository.save(new Store.Builder(request).build());
    }

    /**
     * 가게 조회 복수
     */
    public ListResult<StoreItem> getStores() {
        List<Store> originList =storeRepository.findAll();
        List<StoreItem> result = new LinkedList<>();

        for (Store store : originList) result.add(new StoreItem.Builder(store).build());

        return ListConvertService.settingResult(result);
    }

    /**
     * 가게 조회 단수
     */
    public StoreResponse getStore(long storeId) {
        Store originData = storeRepository.findById(storeId).orElseThrow();

        return new StoreResponse.Builder(originData).build();
}

    /**
     * 가게 정보 변경
     */
    public void putStore(long storeId, StoreChangeRequest request) {
        Store originData = storeRepository.findById(storeId).orElseThrow();

        originData.putStore(request);
        storeRepository.save(originData);
    }

    /**
     * 가게 정보 삭제
     */
    public void delStore(Long storeId) {
        storeRepository.deleteById(storeId);
    }

    /**
     * 가게 조회 복수 페이징
     */
    public ListResult<StoreItem> getStoresP(int pageNum) {
        PageRequest pageRequest = ListConvertService.getPageable(pageNum);
        Page<Store> stores = storeRepository.findAll(pageRequest);
        List<StoreItem> response = new LinkedList<>();
        for (Store store : stores) response.add(new StoreItem.Builder(store).build());

        return ListConvertService.settingResult(response, stores.getTotalElements(), stores.getTotalPages(), stores.getPageable().getPageNumber());
    }
}
