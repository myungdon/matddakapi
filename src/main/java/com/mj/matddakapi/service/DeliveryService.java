package com.mj.matddakapi.service;

import com.mj.matddakapi.entity.*;
import com.mj.matddakapi.enums.ask.StateAsk;
import com.mj.matddakapi.enums.delivery.StateDelivery;
import com.mj.matddakapi.enums.payHistory.PayType;
import com.mj.matddakapi.exception.CDeliveryOneException;
import com.mj.matddakapi.exception.CDeliveryPickException;
import com.mj.matddakapi.model.delivery.DeliveryItem;
import com.mj.matddakapi.model.delivery.DeliveryResponse;
import com.mj.matddakapi.model.delivery.TabCountResponse;
import com.mj.matddakapi.model.generic.ListResult;
import com.mj.matddakapi.repository.*;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class DeliveryService {
    private final DeliveryRepository deliveryRepository;
    private final AskRepository askRepository;
    private final IncomeRepository incomeRepository;
    private final PayHistoryRepository payHistoryRepository;
    private final RiderPayRepository riderPayRepository;

    /**
     * fk용 데이터
     */
    public Delivery getData(long riderId) {
        return deliveryRepository.findById(riderId).orElseThrow();
    }

    /**
     * 배차
     */
    public void setDelivery(Rider rider, Ask ask) {
        Delivery addData = new Delivery.Builder(rider, ask).build();
        long pickCount = deliveryRepository.countByRiderAndStateDelivery(rider, StateDelivery.R_PICK);
        long goCount = deliveryRepository.countByRiderAndStateDelivery(rider, StateDelivery.GO);

        if (addData.getAsk().getStateAsk() == StateAsk.S_PICK) throw new CDeliveryPickException(); // 다른 사람이 이미 배차한거면 예외 처리

        if (pickCount >= 1 || goCount >= 1) throw new CDeliveryOneException(); // 본인이 배차중이면 예외처리

        deliveryRepository.save(addData);

        Ask originData = askRepository.findById(addData.getAsk().getId()).orElseThrow();
        originData.putAskPick();

        askRepository.save(originData);
    }

    /**
     * 배송 출발로 변경
     */
    public void putGo(Rider rider) {
        Delivery originData = deliveryRepository.findByStateDeliveryAndRider(StateDelivery.R_PICK, rider).orElseThrow();
        originData.putGo();

        deliveryRepository.save(originData);
    }

    /**
     * 배송 완료로 변경
     */
    public void putDone(Rider rider) {
        Delivery originData1 = deliveryRepository.findByStateDeliveryAndRider(StateDelivery.GO, rider).orElseThrow();
        originData1.putDone();

        deliveryRepository.save(originData1);

        Income addData1 = new Income();
        addData1.setDelivery(originData1);
        addData1.setFeeTotal(originData1.getAsk().getPriceRide() * 0.6);
        addData1.setFeeRider(addData1.getFeeTotal() * 0.88);
        addData1.setFeeAdmin(addData1.getFeeTotal() * 0.1);
        addData1.setTaxSan(addData1.getFeeTotal() * 0.01);
        addData1.setTaxGo(addData1.getFeeTotal() * 0.01);
        addData1.setDateIncome(LocalDate.now());
        addData1.setTimeIncome(LocalDateTime.now());

        incomeRepository.save(addData1);

        PayHistory addData2 = new PayHistory();
        addData2.setRider(originData1.getRider());
        addData2.setPayType(PayType.IN);
        addData2.setPayAmount(addData1.getFeeRider());
        addData2.setDatePayRenewal(LocalDateTime.now());

        payHistoryRepository.save(addData2);

        RiderPay originData2 = riderPayRepository.findById(addData2.getRider().getId()).orElseThrow();
        originData2.putPayNow(originData2.getPayNow() + addData2.getPayAmount());

        riderPayRepository.save(originData2);

        Ask originData3 = askRepository.findById(originData1.getAsk().getId()).orElseThrow();
        originData3.putAskDone();

        askRepository.save(originData3);
    }

    /**
     * 배차 요청으로 변경
     */
    public void putCancel(Rider rider) {
        Delivery originData = deliveryRepository.findByStateDeliveryAndRider(StateDelivery.R_PICK, rider).orElseThrow();
        originData.putCancel();

        deliveryRepository.save(originData);

        Ask originData2 = askRepository.findById(originData.getAsk().getId()).orElseThrow();
        originData2.putAskRequesting();

        askRepository.save(originData2);
    }

    /**
     * 배송 복수 최신순
     */
    public ListResult<DeliveryItem> getDeliveries() {
        List<Delivery> originList = deliveryRepository.findAllByOrderByIdDesc();
        List<DeliveryItem> result = new LinkedList<>();

        for (Delivery delivery : originList) result.add(new DeliveryItem.Builder(delivery).build());

        return ListConvertService.settingResult(result);
    }

    /**
     * 배송 복수 최신순 페이징
     */
    public ListResult<DeliveryItem> getDeliveriesP(int pageNum) {
        PageRequest pageRequest = ListConvertService.getPageable(pageNum);
        Page<Delivery> deliveries = deliveryRepository.findAllByOrderByIdDesc(pageRequest);

        List<DeliveryItem> response = new LinkedList<>();
        for (Delivery delivery : deliveries) response.add(new DeliveryItem.Builder(delivery).build());

        return ListConvertService.settingResult(response, deliveries.getTotalElements(), deliveries.getTotalPages(), deliveries.getPageable().getPageNumber());
    }

    /**
     * 배송 복수 배차만 최신순
     */
    public ListResult<DeliveryItem> getPick() {
        List<Delivery> originList = deliveryRepository.findAllByStateDeliveryOrderByIdDesc(StateDelivery.R_PICK);
        List<DeliveryItem> result = new LinkedList<>();

        for (Delivery delivery : originList) result.add(new DeliveryItem.Builder(delivery).build());
        return ListConvertService.settingResult(result);
    }

    /**
     * 배송 복수 배차만 최신순 페이징
     */
    public ListResult<DeliveryItem> getPickP(int pageNum) {
        PageRequest pageRequest = ListConvertService.getPageable(pageNum);
        Page<Delivery> deliveries = deliveryRepository.findAllByStateDeliveryOrderByIdDesc(StateDelivery.R_PICK, pageRequest);

        List<DeliveryItem> response = new LinkedList<>();
        for (Delivery delivery : deliveries) response.add(new DeliveryItem.Builder(delivery).build());

        return ListConvertService.settingResult(response, deliveries.getTotalElements(), deliveries.getTotalPages(), deliveries.getPageable().getPageNumber());
    }

    /**
     * 배송 복수 출발만 최신순
     */
    public ListResult<DeliveryItem> getGo() {
        List<Delivery> originList = deliveryRepository.findAllByStateDeliveryOrderByIdDesc(StateDelivery.GO);
        List<DeliveryItem> result = new LinkedList<>();

        for (Delivery delivery : originList) result.add(new DeliveryItem.Builder(delivery).build());
        return ListConvertService.settingResult(result);
    }

    /**
     * 배송 복수 출발만 최신순 페이징
     */
    public ListResult<DeliveryItem> getGoP(int pageNum) {
        PageRequest pageRequest = ListConvertService.getPageable(pageNum);
        Page<Delivery> deliveries = deliveryRepository.findAllByStateDeliveryOrderByIdDesc(StateDelivery.GO, pageRequest);

        List<DeliveryItem> response = new LinkedList<>();
        for (Delivery delivery : deliveries) response.add(new DeliveryItem.Builder(delivery).build());

        return ListConvertService.settingResult(response, deliveries.getTotalElements(), deliveries.getTotalPages(), deliveries.getPageable().getPageNumber());
    }

    /**
     * 배송 복수 완료만 최신순
     */
    public ListResult<DeliveryItem> getDone() {
        List<Delivery> originList = deliveryRepository.findAllByStateDeliveryOrderByIdDesc(StateDelivery.DONE);
        List<DeliveryItem> result = new LinkedList<>();

        for (Delivery delivery : originList) result.add(new DeliveryItem.Builder(delivery).build());
        return ListConvertService.settingResult(result);
    }

    /**
     * 배송 복수 완료만 하루치 라이더
     */
    public ListResult<DeliveryItem> getDoneApp(Rider rider) {
        List<Delivery> originList = deliveryRepository.findAllByStateDeliveryAndRiderAndDateDoneOrderByIdDesc(StateDelivery.DONE, rider, LocalDate.now());
        List<DeliveryItem> result = new LinkedList<>();

        for (Delivery delivery : originList) result.add(new DeliveryItem.Builder(delivery).build());
        return ListConvertService.settingResult(result);
    }

    /**
     * 배송 복수 완료만 최신순 페이징
     */
    public ListResult<DeliveryItem> getDoneP(int pageNum) {
        PageRequest pageRequest = ListConvertService.getPageable(pageNum);
        Page<Delivery> deliveries = deliveryRepository.findAllByStateDeliveryOrderByIdDesc(StateDelivery.DONE, pageRequest);

        List<DeliveryItem> response = new LinkedList<>();
        for (Delivery delivery : deliveries) response.add(new DeliveryItem.Builder(delivery).build());

        return ListConvertService.settingResult(response, deliveries.getTotalElements(), deliveries.getTotalPages(), deliveries.getPageable().getPageNumber());
    }

//    /**
//     * 배송 복수 취소만 최신순
//     */
//    public ListResult<DeliveryItem> getCancel() {
//        List<Delivery> originList = deliveryRepository.findAllByStateDeliveryOrderByIdDesc(StateDelivery.R_CANCEL);
//        List<DeliveryItem> result = new LinkedList<>();
//
//        for (Delivery delivery : originList) result.add(new DeliveryItem.Builder(delivery).build());
//        return ListConvertService.settingResult(result);
//    }

//    /**
//     * 배송 복수 취소만 최신순 페이징
//     */
//    public ListResult<DeliveryItem> getCancelP(int pageNum) {
//        PageRequest pageRequest = ListConvertService.getPageable(pageNum);
//        Page<Delivery> deliveries = deliveryRepository.findAllByStateDeliveryOrderByIdDesc(StateDelivery.R_CANCEL, pageRequest);
//
//        List<DeliveryItem> response = new LinkedList<>();
//        for (Delivery delivery : deliveries) response.add(new DeliveryItem.Builder(delivery).build());
//        return ListConvertService.settingResult(response, deliveries.getTotalElements(), deliveries.getTotalPages(), deliveries.getPageable().getPageNumber());
//    }

    /**
     * 배송 단수 (관리자용)
     */

    public DeliveryResponse getDelivery(long deliveryId){
        Delivery originData = deliveryRepository.findById(deliveryId).orElseThrow();

        return new DeliveryResponse.Builder(originData).build();
    }

    /**
     * 배송 배차용 단수 (라이더용)
     */

    public DeliveryResponse getDeliveryPick(Rider rider){
        Delivery originData = deliveryRepository.findByStateDeliveryAndRider(StateDelivery.R_PICK, rider).orElseThrow();

        return new DeliveryResponse.Builder(originData).build();
    }

    /**
     * 배송 출발용 단수 (라이더용)
     */

    public DeliveryResponse getDeliveryGo(Rider rider){
        Delivery originData = deliveryRepository.findByStateDeliveryAndRider(StateDelivery.GO, rider).orElseThrow();

        return new DeliveryResponse.Builder(originData).build();
    }

    /**
     * 배송 완료용 단수 (라이더용)
     */

    public DeliveryResponse getDeliveryDone(long deliveryId, Rider rider){
        Delivery originData = deliveryRepository.findByIdAndRider(deliveryId, rider);

        return new DeliveryResponse.Builder(originData).build();
    }

    /**
     * 탭바
     * @param rider
     * @return
     */

    public TabCountResponse getTabCount(Rider rider) {
        long requestCount = askRepository.countByStateAsk(StateAsk.REQUESTING);
        long pickCount = deliveryRepository.countByStateDeliveryAndRider(StateDelivery.R_PICK, rider);
        long goCount = deliveryRepository.countByStateDeliveryAndRider(StateDelivery.GO, rider);
        long doneCount = deliveryRepository.countByStateDeliveryAndDateDoneAndRider(StateDelivery.DONE, LocalDate.now(), rider);
        long cancelCount = askRepository.countByStateAskAndDateAsk(StateAsk.S_CANCEL, LocalDate.now());

        TabCountResponse response = new TabCountResponse();

        response.setRequestCount(requestCount);
        response.setPickCount(pickCount);
        response.setGoCount(goCount);
        response.setDoneCount(doneCount);
        response.setCancelCount(cancelCount);

        return response;
    }
}
