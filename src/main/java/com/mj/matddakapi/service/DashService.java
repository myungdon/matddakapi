package com.mj.matddakapi.service;

import com.mj.matddakapi.entity.Board;
import com.mj.matddakapi.entity.Rider;
import com.mj.matddakapi.model.board.BoardItem;
import com.mj.matddakapi.model.dash.DashResponse;
import com.mj.matddakapi.model.rider.RiderItem;
import com.mj.matddakapi.model.statistics.NearDayStatisticsResponse;
import com.mj.matddakapi.repository.BoardRepository;
import com.mj.matddakapi.repository.RiderRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class DashService {
    private final RiderRepository riderRepository;
    private final BoardRepository boardRepository;

    public DashResponse getDash() {
        LocalDate today = LocalDate.now();
        LocalDate startDay = today.minusDays(9);
        List<Rider> riders = riderRepository.findAllByOrderByIdDesc();
        List<Board> boards = boardRepository.findAllByOrderByIdDesc();

        List<String> labels = new LinkedList<>();
        List<Long> datasets = new LinkedList<>();
        for (int i = 0; i < 10; i++) {
            labels.add(startDay.plusDays(i).toString());
            datasets.add(riderRepository.countByDateJoin(startDay.plusDays(i)));
        }

        NearDayStatisticsResponse nearDay = new NearDayStatisticsResponse.Builder(labels, datasets).build();

        List<RiderItem> riderList = new LinkedList<>();
        for (Rider rider : riders.subList(0,Math.min(8, riders.size()))) riderList.add(new RiderItem.Builder(rider).build());

        List<BoardItem> boardList = new LinkedList<>();
        for (Board board : boards.subList(0,Math.min(8, boards.size()))) boardList.add(new BoardItem.Builder(board).build());

        return new DashResponse.Builder(nearDay, riderList, boardList).build();
    }
}
