package com.mj.matddakapi.service;

import com.mj.matddakapi.entity.Attendance;
import com.mj.matddakapi.entity.Rider;
import com.mj.matddakapi.model.attendance.AttendanceResponse;
import com.mj.matddakapi.repository.AttendanceRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
public class AttendanceService {
    private final AttendanceRepository attendanceRepository;

    /**
     * 근태 단수
     */
    public AttendanceResponse getAttendance(Rider rider) {
        Attendance originData = attendanceRepository.findByRider_Id(rider.getId());

        return new AttendanceResponse.Builder(originData).build();
    }

    /**
     * 근태 상태 변경
     */
    public void putAttendance(long attendanceId) {
        Attendance originData = attendanceRepository.findById(attendanceId).orElseThrow();
        originData.putAttendance();

        attendanceRepository.save(originData);
    }
}
