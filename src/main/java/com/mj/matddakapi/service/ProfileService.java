package com.mj.matddakapi.service;

import com.mj.matddakapi.entity.Rider;
import com.mj.matddakapi.repository.RiderRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ProfileService {
    private final RiderRepository riderRepository;

    public Rider getData() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String email = authentication.getName();
        return riderRepository.findByEmail(email).orElseThrow(); // 회원정보 없습니다.
    }
}
