package com.mj.matddakapi.service;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.storage.Blob;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Service
public class GCSService {
    @Value("${spring.cloud.gcp.storage.bucket}")
    private String bucketName;

    private String SECRET_KEY = "precise-tube-413600-78638ea254e8.json";

    public String uploadObject(MultipartFile file) throws IOException {
        InputStream keyFile = ResourceUtils.getURL("classpath:" + SECRET_KEY).openStream();


        Storage storage = StorageOptions.newBuilder()
                .setCredentials(GoogleCredentials.fromStream(keyFile))
                .build()
                .getService();

        BlobInfo blobInfo = BlobInfo.newBuilder(bucketName, file.getOriginalFilename())
                .setContentType(file.getContentType()).build();

        Blob blob = storage.create(blobInfo, file.getInputStream());

        return "https://storage.googleapis.com/" + bucketName + "/" + file.getOriginalFilename() + LocalDateTime.now().toString().substring(0, 19);
    }
}
