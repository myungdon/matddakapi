package com.mj.matddakapi.service;

import com.mj.matddakapi.entity.Attendance;
import com.mj.matddakapi.entity.Rider;
import com.mj.matddakapi.entity.RiderInfo;
import com.mj.matddakapi.entity.RiderPay;
import com.mj.matddakapi.enums.rider.Admin;
import com.mj.matddakapi.exception.*;
import com.mj.matddakapi.lib.CommonCheck;
import com.mj.matddakapi.model.attendance.AttendanceResponse;
import com.mj.matddakapi.model.generic.ListResult;
import com.mj.matddakapi.model.rider.*;
import com.mj.matddakapi.model.rider.person.RiderImgChangeRequest;
import com.mj.matddakapi.model.rider.person.RiderPersonChangeRequest;
import com.mj.matddakapi.model.rider.person.RiderPhoneTypeChangeRequest;
import com.mj.matddakapi.model.rider.person.RiderTypeChangeRequest;
import com.mj.matddakapi.model.riderpay.RiderPayResponse;
import com.mj.matddakapi.repository.AttendanceRepository;
import com.mj.matddakapi.repository.RiderInfoRepository;
import com.mj.matddakapi.repository.RiderPayRepository;
import com.mj.matddakapi.repository.RiderRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.LocalDate;
import java.util.*;

@Service
@RequiredArgsConstructor

public class RiderService {
    private final RiderRepository riderRepository;
    private final AttendanceRepository attendanceRepository;
    private final RiderPayRepository riderPayRepository;
    private final PasswordEncoder passwordEncoder;
    private final RiderInfoRepository riderInfoRepository;
    private final GCSService gcsService;

    // 관리자는 관리자만 등록가능해야하고 일반회원은 아무나 등록 가능해야한다.
    // 아무나 관리자로 등록가능하면...?
    // 그러니까 컨트롤러에서 일반회원가입, 관리자 등록 이렇게 두개로 나눠서 구현해야한다.
    // 근데 일반회원가입 서비스 따로, 관리자 등록 서비스 따로 이렇게 하면
    // 어짜피 비슷한 코드인데 굳이 이걸 왜 나누나??
    // 그래서 어떤 회원그룹에 가입시킬건지 서비스에서 받고~
    public Rider getData(long riderId) {
        return riderRepository.findById(riderId).orElseThrow();
    }

    /**
     * @param email 회원가입창에서 고객이 작성한 정보
     * @throws Exception 아이디 중복확인했을 때 데이터베이스에서 일치하는 이메일이 없을 경우
     */
    private boolean isNewRider(String email) {
        long dupCount = riderRepository.countByEmail(email);
        return dupCount <= 0;
    }

    /**
     * 회원 등록 부모님 동의서 x 성인 or 관리자
     * @param admin
     * @param request
     */

    public void setRider(Admin admin, RiderJoinRequest request) {
        if (!CommonCheck.checkEmail(request.getEmail())) throw new CEmailFormException(); // 유효한 아이디 형식이 아닙니다 던지기
        if (!request.getPassword().equals(request.getPasswordRe())) throw new CPwDiffException(); // 비밀번호가 일치하지 않습니다 던지기
        if (!isNewRider(request.getEmail())) throw new CEmailSameException(); // 중복된 이메일이 존재합니다 던지기
        if (!CommonCheck.checkIdNum(request.getIdNum())) throw new CIdNumLengthException(); // 주민번호를 확인해주세요 던지기
        if (!CommonCheck.checkPhoneNumber(request.getPhoneNumber())) throw new CPhoneLengthException(); // 핸드폰 번호를 확인해 주세요

        LocalDate today = LocalDate.now();
        int birthYear = Integer.parseInt(request.getIdNum().substring(0, 2));
        char generation = request.getIdNum().charAt(7);

        int age = 0;
        if (generation == '1' || generation == '2') {
            age = today.getYear() - 1900 - birthYear;
        } else if (generation == '3' || generation == '4') {
            age = today.getYear() - 2000 - birthYear;
        }

        if (age < 15) throw new CAgeUnderException(); // 15세 미만 던지기


        request.setPassword(passwordEncoder.encode(request.getPassword()));
        Rider addData1 = new Rider.Builder(admin, request).build();
        riderRepository.save(addData1);

        Attendance addData2 = new Attendance.Builder(addData1).build();
        attendanceRepository.save(addData2);

        RiderPay addData3 = new RiderPay.Builder(addData1).build();
        riderPayRepository.save(addData3);

        RiderInfo addData4 = new RiderInfo();
        addData4.setAttendance(addData2);
        addData4.setRiderPay(addData3);
        riderInfoRepository.save(addData4);
    }

    /**
     * 회원 등록 부모님 동의서 o 15세 이상 ~ 18세 미만
     * @param admin
     * @param request
     * @param parentsFile
     * @throws IOException
     */

    public void setRider(Admin admin, RiderJoinRequest request, MultipartFile parentsFile) throws IOException {
        if (!CommonCheck.checkEmail(request.getEmail())) throw new CEmailFormException(); // 유효한 아이디 형식이 아닙니다 던지기
        if (!request.getPassword().equals(request.getPasswordRe())) throw new CPwDiffException(); // 비밀번호가 일치하지 않습니다 던지기
        if (!isNewRider(request.getEmail())) throw new CEmailSameException(); // 중복된 이메일이 존재합니다 던지기
        if (!CommonCheck.checkIdNum(request.getIdNum())) throw new CIdNumLengthException(); // 주민번호를 확인해주세요 던지기
        if (!CommonCheck.checkPhoneNumber(request.getPhoneNumber())) throw new CPhoneLengthException(); // 핸드폰 번호를 확인해 주세요

        String imgUri = gcsService.uploadObject(parentsFile);

        LocalDate today = LocalDate.now();
        int birthYear = Integer.parseInt(request.getIdNum().substring(0, 2));
        char generation = request.getIdNum().charAt(7);

        int age = 0;
        if (generation == '1' || generation == '2') {
            age = today.getYear() - 1900 - birthYear;
        } else if (generation == '3' || generation == '4') {
            age = today.getYear() - 2000 - birthYear;
        }

        if (age < 15) throw new CAgeUnderException(); // 15세 미만 던지기


        request.setPassword(passwordEncoder.encode(request.getPassword()));
        Rider addData1 = new Rider.Builder(admin, request, imgUri).build();
        riderRepository.save(addData1);

        Attendance addData2 = new Attendance.Builder(addData1).build();
        attendanceRepository.save(addData2);

        RiderPay addData3 = new RiderPay.Builder(addData1).build();
        riderPayRepository.save(addData3);

        RiderInfo addData4 = new RiderInfo();
        addData4.setAttendance(addData2);
        addData4.setRiderPay(addData3);
        riderInfoRepository.save(addData4);
    }

    /**
     * 관리자(웹) 회원 리스트 조회
     */
    public ListResult<RiderItem> getRiders() {
        List<Rider> originList = riderRepository.findAll();
        List<RiderItem> result = new LinkedList<>();

        for (Rider rider : originList)
            result.add(new RiderItem.Builder(rider).build());

        return ListConvertService.settingResult(result);
    }

    /**
     * 라이더: 회원 정보 조회 detail 토큰 o
     */
    public RiderResponse getRider(Rider rider) {
        Rider originData1 = riderRepository.findById(rider.getId()).orElseThrow();
        Attendance originData2 = attendanceRepository.findByRider_Id(rider.getId());
        RiderPay originData3 = riderPayRepository.findByRider_Id(rider.getId());

        AttendanceResponse attendance = new AttendanceResponse.Builder(originData2).build();

        RiderPayResponse riderPay = new RiderPayResponse.Builder(originData3).build();


        return new RiderResponse.Builder(originData1, attendance, riderPay).build();
    }

    /**
     *관리자 : 회원 정보 조회 detail 토큰 x
     */
    public RiderResponse getAdminRider(Long id) {
        Rider rider = riderRepository.findById(id).orElseThrow();
        Attendance originData2 = attendanceRepository.findByRider_Id(id);
        RiderPay originData3 = riderPayRepository.findByRider_Id(id);

        AttendanceResponse attendance = new AttendanceResponse.Builder(originData2).build();

        RiderPayResponse riderPay = new RiderPayResponse.Builder(originData3).build();

        return new RiderResponse.Builder(rider, attendance, riderPay).build();
    }

    /**
     * 관리자: 라이더 페이징 1~10페이지
     */
    public ListResult<RiderItem> getPages(int pageNum) {
        PageRequest pageRequest = PageRequest.of(pageNum - 1, 9);
        Page<Rider> riderPages = riderRepository.findAll(pageRequest);

        List<RiderItem> result = new LinkedList<>();

        for (Rider rider : riderPages.getContent())
            result.add(new RiderItem.Builder(rider).build());


        ListResult<RiderItem> result1 = new ListResult<>();
        result1.setList(result);
        result1.setTotalCount(riderPages.getTotalElements());
        result1.setTotalPage(riderPages.getTotalPages());
        result1.setCurrentPage(riderPages.getPageable().getPageNumber());

        return result1;
    }
    /**
     * 관리자: 회원정보 수정
     */
    public void putRiders(long id, RiderChangeRequest request){
        Rider originData = riderRepository.findById(id).orElseThrow();
        originData.putRiders(request);

        riderRepository.save(originData);
    }

    /**
     * 라이더: 개인정보 수정 파일 x
     */
    public void putRider(Rider rider, RiderPersonChangeRequest request) {
        Rider originData = riderRepository.findById(rider.getId()).orElseThrow();
        originData.putRider(request);

        riderRepository.save(originData);
    }

    /**
     * 라이더: 개인정보 수정 파일 o
     */
    public void putRider(Rider rider, RiderPersonChangeRequest request, MultipartFile file) throws IOException {
        String imgUri = gcsService.uploadObject(file);
        Rider originData = riderRepository.findById(rider.getId()).orElseThrow();
        originData.putRider(request, imgUri);

        riderRepository.save(originData);
    }
    /**
     * 라이더: 계좌 수정
     */
//    public void putBank(long id, RiderBankChangeRequest request){
//        Rider originData = riderRepository.findById(id).orElseThrow();
//        originData.putBank(request);
//
//        riderRepository.save(originData);
//    }

    /**
     * 라이더: 운송수단 변경
     */
//    public void putRidingType(Rider rider, RiderTypeChangeRequest request){
//        Rider originData = riderRepository.findById(rider.getId()).orElseThrow();
//        originData.putRidingType(request);
//
//        riderRepository.save(originData);
//    }

    /**
     * 라이더 블랙 리스트
     * @return
     */

    public ListResult<RiderItem> getBlackList(){
        List<Rider> originList = riderRepository.findAllByIsBan(true);
        List<RiderItem> result = new LinkedList<>();
        for (Rider rider : originList)
            result.add(new RiderItem.Builder(rider).build());

        return ListConvertService.settingResult(result);
    }

    /**
     * 라이더 : 기종 변경
     */
//    public void putPhoneType(Rider rider, RiderPhoneTypeChangeRequest request) {
//        Rider originData = riderRepository.findById(rider.getId()).orElseThrow();
//        originData.putPhoneType(request);
//
//        riderRepository.save(originData);
//    }

    /**
     * 라이더 : 이미지 변경
     */
    public void putRiderImg(Rider rider, MultipartFile file) throws IOException {
        String imgUri = gcsService.uploadObject(file);
        rider.putRiderImg(imgUri);

        riderRepository.save(rider);
    }
}
