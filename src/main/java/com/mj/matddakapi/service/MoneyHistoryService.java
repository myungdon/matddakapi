package com.mj.matddakapi.service;

import com.mj.matddakapi.entity.MoneyHistory;
import com.mj.matddakapi.entity.PayHistory;
import com.mj.matddakapi.entity.Rider;
import com.mj.matddakapi.entity.RiderPay;
import com.mj.matddakapi.enums.moneyHistory.MoneyType;
import com.mj.matddakapi.enums.payHistory.PayType;
import com.mj.matddakapi.exception.CMoneyOutOverException;
import com.mj.matddakapi.model.generic.ListResult;
import com.mj.matddakapi.model.moneyhistory.MoneyHistoryCreateRequest;
import com.mj.matddakapi.model.moneyhistory.MoneyHistoryItem;
import com.mj.matddakapi.model.moneyhistory.MoneyHistoryResponse;
import com.mj.matddakapi.repository.MoneyHistoryRepository;
import com.mj.matddakapi.repository.PayHistoryRepository;
import com.mj.matddakapi.repository.RiderPayRepository;
import com.mj.matddakapi.repository.RiderRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class MoneyHistoryService {
    private final RiderRepository riderRepository;
    private final MoneyHistoryRepository moneyHistoryRepository;
    private final PayHistoryRepository payHistoryRepository;
    private final RiderPayRepository riderPayRepository;

    /**
     * 입금 신청
     */
    public void setMoneyIn(Rider rider, MoneyHistoryCreateRequest request) {
        MoneyHistory addData1 = new MoneyHistory();
        addData1.setRider(rider);
        addData1.setMoneyType(MoneyType.IN);
        addData1.setMoneyAmount(request.getMoneyAmount());
        addData1.setDateMoneyRenewal(LocalDateTime.now());
        addData1.setEtcMemo(request.getEtcMemo());

        moneyHistoryRepository.save(addData1);

        PayHistory addData2 = new PayHistory();
        addData2.setRider(rider);
        addData2.setPayType(PayType.IN);
        addData2.setPayAmount(addData1.getMoneyAmount());
        addData2.setDatePayRenewal(LocalDateTime.now());

        payHistoryRepository.save(addData2);

        RiderPay originData = riderPayRepository.findById(addData1.getRider().getId()).orElseThrow();
        originData.putPayNow(originData.getPayNow() + addData1.getMoneyAmount());

        riderPayRepository.save(originData);
    }

    /**
     * 출금 신청
     */
    public void setMoneyOut(Rider rider, MoneyHistoryCreateRequest request) throws CMoneyOutOverException {
        Optional<Rider> rider1 = riderRepository.findById(rider.getId());
        Optional<RiderPay> riderPay = riderPayRepository.findById(rider1.get().getId());

        if (riderPay.get().getPayNow() < request.getMoneyAmount()) throw new CMoneyOutOverException(); // 출금 금액 초과시 예외 처리
        MoneyHistory addData1 = new MoneyHistory();
        addData1.setRider(rider);
        addData1.setMoneyType(MoneyType.OUT);
        addData1.setMoneyAmount(request.getMoneyAmount());
        addData1.setDateMoneyRenewal(LocalDateTime.now());
        addData1.setEtcMemo(request.getEtcMemo());

        moneyHistoryRepository.save(addData1);

        PayHistory addData2 = new PayHistory();
        addData2.setRider(rider);
        addData2.setPayType(PayType.OUT);
        addData2.setPayAmount(addData1.getMoneyAmount());
        addData2.setDatePayRenewal(LocalDateTime.now());

        payHistoryRepository.save(addData2);

        RiderPay originData = riderPayRepository.findById(addData1.getRider().getId()).orElseThrow();
        originData.putPayNow(originData.getPayNow() - addData1.getMoneyAmount());

        riderPayRepository.save(originData);

    }


    /**
     * 라이더 1명 출금 내역 전체 보기 최신순
     */
    public ListResult<MoneyHistoryItem> getMoneyOuts(Rider rider) {
        List<MoneyHistory> oringinList = moneyHistoryRepository.findByRider_IdAndMoneyTypeOrderByIdDesc(rider.getId(), MoneyType.OUT);
        List<MoneyHistoryItem> result = new LinkedList<>();

        for (MoneyHistory moneyHistory : oringinList) result.add(new MoneyHistoryItem.Builder(moneyHistory).build());

        return ListConvertService.settingResult(result);
    }

    /**
     * 라이더 입출 내역 단수
     */
    public MoneyHistoryResponse getMoneyHistory(long moneyId) {
        MoneyHistory originData = moneyHistoryRepository.findById(moneyId).orElseThrow();

        return new MoneyHistoryResponse.Builder(originData).build();
    }

    /**
     * 라이더 전체 출금 내역 전체 보기 최신순 페이징
     */
    public ListResult<MoneyHistoryItem> getMoneyOutsP(int pageNum) {
        PageRequest pageRequest = ListConvertService.getPageable(pageNum);
        Page<MoneyHistory> moneyHistories = moneyHistoryRepository.findAllByMoneyTypeOrderByIdDesc(MoneyType.OUT, pageRequest);

        List<MoneyHistoryItem> response = new LinkedList<>();
        for (MoneyHistory moneyHistory : moneyHistories) response.add(new MoneyHistoryItem.Builder(moneyHistory).build());

        return ListConvertService.settingResult(response, moneyHistories.getTotalElements(), moneyHistories.getTotalPages(), moneyHistories.getPageable().getPageNumber());
    }
}
