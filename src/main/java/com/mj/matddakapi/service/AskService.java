package com.mj.matddakapi.service;

import com.mj.matddakapi.entity.Ask;
import com.mj.matddakapi.entity.Store;
import com.mj.matddakapi.enums.ask.StateAsk;
import com.mj.matddakapi.model.ask.AskCreateRequest;
import com.mj.matddakapi.model.ask.AskItem;
import com.mj.matddakapi.model.ask.AskResponse;
import com.mj.matddakapi.model.generic.ListResult;
import com.mj.matddakapi.repository.AskRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class AskService {
    private final AskRepository askRepository;

    /**
     * fk용 데이터
     */
    public Ask getData(long askId){
        return askRepository.findById(askId).orElseThrow();
    }


    /**
     * 주문 등록
     */
    public void setAsk(Store store, AskCreateRequest request) {

        askRepository.save(new Ask.Builder(store, request).build());
    }

    /**
     * 주문 복수 최신순
     */
    public ListResult<AskItem> getAsks() {
        List<Ask> originList = askRepository.findAllByOrderByIdDesc();
        List<AskItem> result = new LinkedList<>();

        for (Ask ask : originList) result.add(new AskItem.Builder(ask).build());
        return ListConvertService.settingResult(result);
    }

    /**
     * 주문 복수 10km 이내 요청상태만 최신순
     */
    public ListResult<AskItem> getAsksRe() {
        List<Ask> originList = askRepository.findAllByStateAskAndDistanceLessThanEqualOrderByIdDesc(StateAsk.REQUESTING, 10);
        List<AskItem> result = new LinkedList<>();

        for (Ask ask : originList) result.add(new AskItem.Builder(ask).build());
        return ListConvertService.settingResult(result);
    }

    /**
     * 주문 복수 취소 상태만 최신순 하루치
     */
    public ListResult<AskItem> getAsksCanDay() {
        LocalDate today = LocalDate.now();
        List<Ask> originList = askRepository.findAllByStateAskAndDateAskOrderByIdDesc(StateAsk.S_CANCEL, today);
        List<AskItem> result = new LinkedList<>();

        for (Ask ask : originList) result.add(new AskItem.Builder(ask).build());
        return ListConvertService.settingResult(result);
    }

    /**
     * 주문 단수
     */
    public AskResponse getAsk(long askId) {
        Ask originData = askRepository.findById(askId).orElseThrow();

       return new AskResponse.Builder(originData).build();
    }

    /**
     *주문 변경 취소로 변경
     */
    public void putAskCancel(Long askId) {
        Ask originData = askRepository.findById(askId).orElseThrow();
        originData.putAskCancel();
        askRepository.save(originData);
    }

     /**
     * 주문 복수 요청상태만 최신순 페이징
     */
    public ListResult<AskItem> getAskReP(int pageNum) {
        PageRequest pageRequest = ListConvertService.getPageable(pageNum);
        Page<Ask> asks = askRepository.findAllByStateAskOrderByIdDesc(StateAsk.REQUESTING, pageRequest);

        List<AskItem> response = new LinkedList<>();
        for (Ask ask : asks) response.add(new AskItem.Builder(ask).build());

        return ListConvertService.settingResult(response, asks.getTotalElements(), asks.getTotalPages(), asks.getPageable().getPageNumber());
    }

    /**
     * 주문 복수 취소상태만 최신순 페이징
     */
    public ListResult<AskItem> getAskCanP(int pageNum) {
        PageRequest pageRequest = ListConvertService.getPageable(pageNum);
        Page<Ask> asks = askRepository.findAllByStateAskOrderByIdDesc(StateAsk.S_CANCEL, pageRequest);

        List<AskItem> response = new LinkedList<>();
        for (Ask ask : asks) response.add(new AskItem.Builder(ask).build());

        return ListConvertService.settingResult(response, asks.getTotalElements(), asks.getTotalPages(), asks.getPageable().getPageNumber());
    }

    /**
     * 주문 복수 요청+요청취소 상태만 최신순 페이징
     */
    public ListResult<AskItem> getAskReCanP(int pageNum) {
        PageRequest pageRequest = ListConvertService.getPageable(pageNum);
        Page<Ask> asks = askRepository.findAllByStateAskOrStateAskOrderByIdDesc(StateAsk.REQUESTING, StateAsk.S_CANCEL, pageRequest);

        List<AskItem> response = new LinkedList<>();
        for (Ask ask : asks) response.add(new AskItem.Builder(ask).build());

        return ListConvertService.settingResult(response, asks.getTotalElements(), asks.getTotalPages(), asks.getPageable().getPageNumber());
    }
}