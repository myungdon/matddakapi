package com.mj.matddakapi.repository;

import com.mj.matddakapi.entity.RiderInfo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RiderInfoRepository extends JpaRepository<RiderInfo, Long> {
}
