package com.mj.matddakapi.repository;

import com.mj.matddakapi.entity.RiderPay;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RiderPayRepository extends JpaRepository<RiderPay, Long> {

    // 라이더페이 복수 최신순으로 페이징
    Page<RiderPay> findAllByOrderByIdAsc(Pageable pageable);

    RiderPay findByRider_Id(Long riderId);
}
