package com.mj.matddakapi.repository;

import com.mj.matddakapi.entity.Attendance;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AttendanceRepository extends JpaRepository<Attendance, Long> {
    /**
     * 근태 단수용
     */
    Attendance findByRider_Id(long riderId);
}
