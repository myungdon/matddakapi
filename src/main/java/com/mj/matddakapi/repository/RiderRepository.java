package com.mj.matddakapi.repository;

import com.mj.matddakapi.entity.Rider;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface RiderRepository extends JpaRepository<Rider, Long> {
    long countByEmail(String email);
    Optional<Rider> findByEmailAndPassword(String email,String password);

    List<Rider> findAllByIsBan(Boolean isBan);

    long countByDateJoin(LocalDate dateJoin);

    Optional<Rider> findByEmail(String email);

    List<Rider> findAllByOrderByIdDesc();
}