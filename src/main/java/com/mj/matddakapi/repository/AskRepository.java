package com.mj.matddakapi.repository;

import com.mj.matddakapi.entity.Ask;
import com.mj.matddakapi.enums.ask.StateAsk;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


import java.time.LocalDate;
import java.util.List;

public interface AskRepository extends JpaRepository<Ask, Long> {

    // 복수 최신순으로
    List<Ask> findAllByOrderByIdDesc();

    // 복수 상태 같으면서 일정 거리 이내 최신순으로
    List<Ask> findAllByStateAskAndDistanceLessThanEqualOrderByIdDesc(StateAsk stateAsk, double distance);

    // 복수 상태 같으면서 최신순으로 하루치
    List<Ask> findAllByStateAskAndDateAskOrderByIdDesc(StateAsk stateAsk, LocalDate dateAsk);

    // 복수 상태 같으면서 최신순으로 페이징
    Page<Ask> findAllByStateAskOrderByIdDesc(StateAsk stateAsk, Pageable pageable);

    // 복수 상태 요청or요청완료만 최신순으로 페이징
    Page<Ask> findAllByStateAskOrStateAskOrderByIdDesc(StateAsk stateAsk1, StateAsk stateAsk2, Pageable pageable);

    /**
     * 하루치 요청상태 같은거 데이터
     * @param dateCancel
     * @param stateAsk
     * @return
     */
    List<Ask> findAllByDateAskAndStateAsk(LocalDate dateCancel, StateAsk stateAsk);

    long countByStateAsk(StateAsk stateAsk);

    long countByStateAskAndDateAsk(StateAsk stateAsk, LocalDate dateAsk);
}
