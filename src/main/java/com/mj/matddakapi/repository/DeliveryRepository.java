package com.mj.matddakapi.repository;

import com.mj.matddakapi.entity.Delivery;
import com.mj.matddakapi.entity.Rider;
import com.mj.matddakapi.enums.delivery.StateDelivery;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface DeliveryRepository extends JpaRepository<Delivery, Long> {

    // 배송 복수 최신순
    List<Delivery> findAllByOrderByIdDesc();

    // 배송 복수 최신순 페이징
    Page<Delivery> findAllByOrderByIdDesc(Pageable pageable);

    // 배송 복수 상태 같으면서 최신순
    List<Delivery> findAllByStateDeliveryOrderByIdDesc(StateDelivery stateDelivery);

    // 배송 복수 상태 같으면서 최신순 페이징
    Page<Delivery> findAllByStateDeliveryOrderByIdDesc(StateDelivery stateDelivery, Pageable pageable);

    // 배송 상태 같은거 갯수 token
    long countByStateDeliveryAndRider(StateDelivery stateDelivery, Rider rider);

    // 배송 상태 같으면서 하루치 갯수 token
    long countByStateDeliveryAndDateDoneAndRider(StateDelivery stateDelivery, LocalDate dateDone, Rider rider);

    // 배송 앱용 단수 token
    Delivery findByAsk_IdAndRider(Long id, Rider rider);

    // 배송 상태 같은거 하루치 최신순 token
    List<Delivery> findAllByStateDeliveryAndRiderAndDateDoneOrderByIdDesc(StateDelivery stateDelivery, Rider rider, LocalDate dateDone);

    // 라이더가 배차신청 1개만 되게 예외처리용
    long countByRiderAndStateDelivery(Rider rider, StateDelivery stateDelivery);

    // 배차에서 ask-id 로 찾아 오는거
    Optional<Delivery> findByAsk_Id(Long askId);

//    // 배차 상태 같은거 단수 token
//    Optional<Delivery> findByStateDeliveryAndRider(StateDelivery stateDelivery, Rider rider);

    // 배차 상태 같은거 id로 단수 token
    Delivery findByIdAndRider(Long id, Rider rider);

    // 배송에서 상태 변경
    Optional<Delivery> findByStateDeliveryAndRider(StateDelivery stateDelivery, Rider rider);
}