package com.mj.matddakapi.entity;

import com.mj.matddakapi.interfaces.CommonModelBuilder;
import com.mj.matddakapi.model.board.BoardChangeRequest;
import com.mj.matddakapi.model.board.BoardCreateRequest;
import jakarta.persistence.*;
import lombok.*;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Board {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 50)
    private String title;

    @Column(length = 100)
    private String img;

    @Column(nullable = false)
    private String text;

    @Column(nullable = false)
    private LocalDate dateCreate;

    /**
     * 게시글 수정 이미지 o
     */

    public void putBoard(BoardChangeRequest request, String imgUri) {
        this.title = request.getTitle();
        this.img = imgUri;
        this.text = request.getText();
        this.dateCreate = LocalDate.now();
    }

    /**
     * 게시글 수정 이미지 x
     */

    public void putBoard(BoardChangeRequest request) {
        this.title = request.getTitle();
        this.img = getImg() == null ? null : getImg();
        this.text = request.getText();
        this.dateCreate = LocalDate.now();
    }


    /**
     * 게시글 등록
     * @param builder
     */

    public Board(Builder builder) {
        this.title = builder.title;
        this.img = builder.img;
        this.text = builder.text;
        this.dateCreate = builder.dateCreate;
    }

    public static class Builder implements CommonModelBuilder<Board> {
        private final String title;
        private String img;
        private final String text;
        private final LocalDate dateCreate;

        /**
         * 등록 이미지 o
         * @param request
         * @param imgUri
         */

        public Builder(BoardCreateRequest request, String imgUri) {
            this.title = request.getTitle();
            this.img = imgUri;
            this.text = request.getText();
            this.dateCreate = LocalDate.now();
        }

        /**
         * 등록 이미지 x
         * @param request
         */

        public Builder(BoardCreateRequest request) {
            this.title = request.getTitle();
            this.text = request.getText();
            this.dateCreate = LocalDate.now();
        }

        @Override
        public Board build() {
            return new Board(this);
        }

    }
}
