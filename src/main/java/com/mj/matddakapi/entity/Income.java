package com.mj.matddakapi.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
public class Income {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER) // lazy 확인
    @JoinColumn(name = "deliveryId", nullable = false)
    private Delivery delivery;

    @Column(nullable = false)
    private Double feeTotal;

    @Column(nullable = false)
    private Double feeRider;

    @Column(nullable = false)
    private Double feeAdmin;

    @Column(nullable = false)
    private Double taxSan;

    @Column(nullable = false)
    private Double taxGo;

    @Column(nullable = false)
    private LocalDate dateIncome;

    @Column(nullable = false)
    private LocalDateTime timeIncome;
}
