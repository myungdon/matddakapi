package com.mj.matddakapi.entity;

import com.mj.matddakapi.enums.rider.*;
import com.mj.matddakapi.interfaces.CommonModelBuilder;
import com.mj.matddakapi.model.rider.RiderChangeRequest;
import com.mj.matddakapi.model.rider.RiderJoinRequest;
import com.mj.matddakapi.model.rider.person.RiderImgChangeRequest;
import com.mj.matddakapi.model.rider.person.RiderPersonChangeRequest;
import com.mj.matddakapi.model.rider.person.RiderPhoneTypeChangeRequest;
import com.mj.matddakapi.model.rider.person.RiderTypeChangeRequest;
import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.multipart.MultipartFile;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Rider implements UserDetails{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 20)
    private String name;

    @Column(nullable = false, length = 20)
    @Enumerated(value = EnumType.STRING)
    private Admin admin;

    @Column(nullable = false, length = 14)
    private String idNum;

    @Column(length = 100)
    private String confirmParents;

    @Column(nullable = false, unique = true, length = 40)
    private String email;

    @Column(nullable = false)
    private String password;

    @Column(nullable = false, unique = true, length = 20)
    private String phoneNumber;

    @Column(nullable = false, length = 20)
    @Enumerated(value = EnumType.STRING)
    private PhoneType phoneType;

    @Column(nullable = false, length = 20)
    private String bankOwner;

    @Column(nullable = false, length = 20)
    @Enumerated(value = EnumType.STRING)
    private BankName bankName;

    @Column(nullable = false, length = 14)
    private String bankIdNum;

    @Column(nullable = false, length = 30)
    private String bankNumber;

    @Column(nullable = false, length = 30)
    @Enumerated(value = EnumType.STRING)
    private AddressWish addressWish;

    @Column(nullable = false, length = 50)
    @Enumerated(value = EnumType.STRING)
    private DriveType driveType;

    @Column(nullable = false, length = 20)
    private String driveNumber;

    @Column(nullable = false)
    private LocalDate dateJoin;

    @Column(nullable = false)
    private Boolean isBan;

    @Enumerated(value = EnumType.STRING)
    @Column(length = 50)
    private ReasonBan reasonBan;

    @Column(length = 20)
    private LocalDate dateBan;

    @Column(columnDefinition = "TEXT")
    private String etcMemo;

    @Column(length = 100)
    private String riderImg;



    /**
     * 관리자: 회원정보 수정
     */
    public void putRiders(RiderChangeRequest request) {
        this.admin = request.getAdmin();
        this.email = request.getEmail();
        this.phoneNumber = request.getPhoneNumber();
        this.phoneType = request.getPhoneType();
        this.bankName = request.getBankName();
        this.bankIdNum = request.getBankIdNum();
        this.bankOwner = request.getBankOwner();
        this.bankNumber = request.getBankNumber();
        this.addressWish = request.getAddressWish();
        this.driveType = request.getDriveType();
        this.driveNumber = request.getDriveNumber();
        this.isBan = request.getIsBan();
        this.reasonBan = request.getReasonBan();
        this.dateBan = reasonBan == ReasonBan.NOT_BAN ? null : LocalDate.now();
        this.etcMemo = request.getEtcMemo();
    }

    /**
     * 라이더: 개인정보 수정 이미지 x
     */
    public void putRider(RiderPersonChangeRequest request) {
        this.name = request.getName();
        this.addressWish = request.getAddressWish();
        this.driveType = request.getDriveType();
        this.driveNumber = request.getDriveNumber();
        this.bankOwner = request.getBankOwner();
        this.bankName = request.getBankName();
        this.bankNumber = request.getBankNumber();
        this.riderImg = null;
    }

    /**
     * 라이더: 개인정보 수정 이미지 o
     */
    public void putRider(RiderPersonChangeRequest request, String imgUri) {
        this.name = request.getName();
        this.addressWish = request.getAddressWish();
        this.driveType = request.getDriveType();
        this.driveNumber = request.getDriveNumber();
        this.bankOwner = request.getBankOwner();
        this.bankName = request.getBankName();
        this.bankNumber = request.getBankNumber();
        this.riderImg = imgUri;
    }

    /**
     * 라이더 : 이미지 변경
     */
    public void putRiderImg(String imgUri) {
        this.riderImg = imgUri;
    }

    /**
     * 회원 가입
     * @param builder
     */

    public Rider(Builder builder) {
        this.name = builder.name;
        this.admin = builder.admin;
        this.idNum = builder.idNum;
        this.confirmParents = builder.confirmParents;
        this.email = builder.email;
        this.password = builder.password;
        this.phoneNumber = builder.phoneNumber;
        this.phoneType = builder.phoneType;
        this.bankOwner = builder.bankOwner;
        this.bankName = builder.bankName;
        this.bankIdNum = builder.bankIdNum;
        this.bankNumber = builder.bankNumber;
        this.addressWish = builder.addressWish;
        this.driveType = builder.driveType;
        this.driveNumber = builder.driveNumber;
        this.dateJoin = builder.dateJoin;
        this.isBan = builder.isBan;
        this.reasonBan = builder.reasonBan;
        this.dateBan = builder.dateBan;
        this.etcMemo = builder.etcMemo;
        this.riderImg = builder.riderImg;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.singleton(new SimpleGrantedAuthority(admin.toString()));
    }

    @Override
    public String getUsername() {
        return email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }


    public static class Builder implements CommonModelBuilder<Rider> {
        private final String name;
        private final Admin admin;
        private final String idNum;
        private String confirmParents;
        private final String email;
        private final String password;
        private final String phoneNumber;
        private final PhoneType phoneType;
        private final String bankOwner;
        private final BankName bankName;
        private final String bankIdNum;
        private final String bankNumber;
        private final AddressWish addressWish;
        private final DriveType driveType;
        private final String driveNumber;
        private final LocalDate dateJoin;
        private final Boolean isBan;
        private final ReasonBan reasonBan;
        private  String etcMemo;
        private LocalDate dateBan;
        private String riderImg;

        /**
         * 15세 이상 ~ 18세 미만
         * @param admin
         * @param request
         * @param imgUri
         */

        public Builder(Admin admin, RiderJoinRequest request, String imgUri) {
            LocalDate today = LocalDate.now();

            /**
             * 주민번호 입력시 나이로 치환하는 로직
             */

            int birthYear = Integer.parseInt(request.getIdNum().substring(0, 2));
            char generation = request.getIdNum().charAt(7);

            int age = 0;
            if (generation == '1' || generation == '2') {
                age = today.getYear() - 1900 - birthYear;
            } else if (generation == '3' || generation == '4') {
                age = today.getYear() - 2000 - birthYear;
            }

            this.name = request.getName();
            this.admin = admin;
            this.idNum = request.getIdNum();

            if (age < 18) { // 나이가 18세 미만 일때 실행
                this.confirmParents = imgUri;
                this.isBan = true;
                this.reasonBan = ReasonBan.PARENTAL_CONSENT;
                this.dateBan = today;
            } else {
                this.confirmParents = null;
                this.isBan = false;
                this.reasonBan = ReasonBan.NOT_BAN;
                this.dateBan = null;
            }

            this.email = request.getEmail();
            this.password = request.getPassword();
            this.phoneNumber = request.getPhoneNumber();
            this.phoneType = PhoneType.SMART_PHONE;
            this.bankOwner = request.getBankOwner();
            this.bankName = request.getBankName();
            this.bankIdNum = request.getBankIdNum();
            this.bankNumber = request.getBankNumber();
            this.addressWish = request.getAddressWish();
            this.driveType = request.getDriveType();
            this.driveNumber = request.getDriveNumber();
            this.dateJoin = LocalDate.now();
            this.riderImg = "https://storage.googleapis.com/md-example-bucket/matddak_symbol_gradient.png";
        }

        /**
         * 성인 or 관리자
         * @param admin
         * @param request
         */

        public Builder(Admin admin, RiderJoinRequest request) {
            LocalDate today = LocalDate.now();

            int birthYear = Integer.parseInt(request.getIdNum().substring(0, 2));
            char generation = request.getIdNum().charAt(7);

            int age = 0;
            if (generation == '1' || generation == '2') {
                age = today.getYear() - 1900 - birthYear;
            } else if (generation == '3' || generation == '4') {
                age = today.getYear() - 2000 - birthYear;
            }

            this.name = request.getName();
            this.admin = admin;
            this.idNum = request.getIdNum();

            if (age < 18) { // 나이가 18세 미만 일때 실행
                this.confirmParents = request.getConfirmParents();
                this.isBan = true;
                this.reasonBan = ReasonBan.PARENTAL_CONSENT;
                this.dateBan = today;
            } else {
                this.confirmParents = null;
                this.isBan = false;
                this.reasonBan = ReasonBan.NOT_BAN;
                this.dateBan = null;
            }

            this.email = request.getEmail();
            this.password = request.getPassword();
            this.phoneNumber = request.getPhoneNumber();
            this.phoneType = PhoneType.SMART_PHONE;
            this.bankOwner = request.getBankOwner();
            this.bankName = request.getBankName();
            this.bankIdNum = request.getBankIdNum();
            this.bankNumber = request.getBankNumber();
            this.addressWish = request.getAddressWish();
            this.driveType = request.getDriveType();
            this.driveNumber = request.getDriveNumber();
            this.dateJoin = LocalDate.now();
            this.riderImg = "https://storage.googleapis.com/md-example-bucket/matddak_symbol_gradient.png";
//            this.name = request.getName();
//            this.admin = admin;
//            this.idNum = request.getIdNum();
//            this.confirmParents = null;
//            this.isBan = false;
//            this.reasonBan = ReasonBan.NOT_BAN;
//            this.dateBan = null;
//            this.email = request.getEmail();
//            this.password = request.getPassword();
//            this.phoneNumber = request.getPhoneNumber();
//            this.phoneType = PhoneType.SMART_PHONE;
//            this.bankOwner = request.getBankOwner();
//            this.bankName = request.getBankName();
//            this.bankIdNum = request.getBankIdNum();
//            this.bankNumber = request.getBankNumber();
//            this.addressWish = request.getAddressWish();
//            this.driveType = request.getDriveType();
//            this.driveNumber = request.getDriveNumber();
//            this.dateJoin = LocalDate.now();
//            this.riderImg = null;
        }

        @Override
        public Rider build() {
            return new Rider(this);
        }
    }

    /**
     *
     * @param builder 대량 등록용
     */
//    public Rider(BuilderPer builder) {
//        this.name = builder.name;
//        this.admin = builder.admin;
//        this.idNum = builder.idNum;
//        this.confirmParents = builder.confirmParents;
//        this.email = builder.email;
//        this.password = builder.password;
//        this.phoneNumber = builder.phoneNumber;
//        this.phoneType = builder.phoneType;
//        this.bankOwner = builder.bankOwner;
//        this.bankName = builder.bankName;
//        this.bankIdNum = builder.bankIdNum;
//        this.bankNumber = builder.bankNumber;
//        this.addressWish = builder.addressWish;
//        this.driveType = builder.driveType;
//        this.driveNumber = builder.driveNumber;
//        this.dateJoin = builder.dateJoin;
//        this.isBan = builder.isBan;
//        this.reasonBan = builder.reasonBan;
//        this.dateBan = builder.dateBan;
//        this.etcMemo = builder.etcMemo;
//        this.riderImg =builder.riderImg;
//    }
//
//    public static class BuilderPer implements CommonModelBuilder<Rider> {
//        private final String name;
//        private final Admin admin;
//        private final String idNum;
//        private final String confirmParents;
//        private final String email;
//        private final String password;
//        private final String phoneNumber;
//        private final PhoneType phoneType;
//        private final String bankOwner;
//        private final BankName bankName;
//        private final String bankIdNum;
//        private final String bankNumber;
//        private final AddressWish addressWish;
//        private final DriveType driveType;
//        private final String driveNumber;
//        private final LocalDate dateJoin;
//        private final Boolean isBan;
//        private final ReasonBan reasonBan;
//        private final LocalDate dateBan;
//        private final String etcMemo;
//        private final String riderImg;
//
//        public BuilderPer(String[] cols) {
//            this.name = cols[0];
//            this.admin = Admin.valueOf(cols[1]);
//            this.idNum = cols[2];
//            this.confirmParents = cols[3];
//            this.email = cols[4];
//            this.password = cols[5];
//            this.phoneNumber = cols[6];
//            this.phoneType = PhoneType.valueOf(cols[7]);
//            this.bankOwner = cols[8];
//            this.bankIdNum = cols[9];
//            this.bankName = BankName.valueOf(cols[10]);
//            this.bankNumber = cols[11];
//            this.addressWish = AddressWish.valueOf(cols[12]);
//            this.driveType = DriveType.valueOf(cols[13]);
//            this.driveNumber = cols[14];
//            this.dateJoin = LocalDate.parse(cols[15]);
//            this.isBan = Boolean.valueOf(cols[16]);
//            this.reasonBan = ReasonBan.valueOf(cols[17]);
//            this.dateBan = LocalDate.parse(cols[18]);
//            this.etcMemo = cols[19];
//            this.riderImg = cols[20];
//        }
//
//        @Override
//        public Rider build() {
//            return new Rider(this);
//        }
//    }

}

