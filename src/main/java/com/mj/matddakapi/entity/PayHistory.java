package com.mj.matddakapi.entity;

import com.mj.matddakapi.enums.payHistory.PayType;
import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDateTime;

@Entity
@Getter
@Setter
public class PayHistory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER) // lazy 확인
    @JoinColumn(name = "riderId", nullable = false)
    private Rider rider;

    @Column(nullable = false, length = 20)
    @Enumerated(value = EnumType.STRING)
    private PayType payType;

    @Column(nullable = false)
    private Double payAmount;

    @Column(nullable = false)
    private LocalDateTime datePayRenewal;

}
