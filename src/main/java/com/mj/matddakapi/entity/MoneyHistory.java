package com.mj.matddakapi.entity;

import com.mj.matddakapi.enums.moneyHistory.MoneyType;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Entity
@Getter
@Setter
public class MoneyHistory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER) // lazy 확인
    @JoinColumn(name = "riderId", nullable = false)
    private Rider rider;

    @Column(nullable = false, length = 20)
    @Enumerated(value = EnumType.STRING)
    private MoneyType moneyType;

    @Column(nullable = false)
    private Double moneyAmount;

    @Column(nullable = false)
    private LocalDateTime dateMoneyRenewal;

    @Column(columnDefinition = "TEXT")
    private String etcMemo;
}
