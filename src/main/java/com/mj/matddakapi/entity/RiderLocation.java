package com.mj.matddakapi.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Entity
@Getter
@Setter
public class RiderLocation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER) // lazy 확인
    @JoinColumn(name = "riderId", nullable = false)
    private Rider rider;

    @Column(nullable = false)
    private LocalDateTime dateXYRiderRenewal;

    @Column(nullable = false)
    private Double xRider;

    @Column(nullable = false)
    private Double yRider;
}
