package com.mj.matddakapi.entity;

import com.mj.matddakapi.enums.delivery.StateDelivery;
import com.mj.matddakapi.interfaces.CommonModelBuilder;
import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDate;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Delivery {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER) // lazy 확인
    @JoinColumn(name = "riderId", nullable = false)
    private Rider rider;

    @ManyToOne(fetch = FetchType.EAGER) // lazy 확인
    @JoinColumn(name = "askId", nullable = false)
    private Ask ask;

    @Column(nullable = false)
    @Enumerated(value = EnumType.STRING)
    private StateDelivery stateDelivery;

    @Column(nullable = false)
    private LocalDate datePick;

    private LocalDate dateGo;

    private LocalDate dateDone;

    private LocalDate dateCancel;

    public void putCancel() {

        this.stateDelivery = StateDelivery.R_CANCEL;
        this.dateCancel = LocalDate.now();
    }

    public void putDone() {
        this.stateDelivery = StateDelivery.DONE;
        this.dateDone = LocalDate.now();
    }

    public void putGo() {
        this.stateDelivery = StateDelivery.GO;
        this.dateGo = LocalDate.now();
    }

    public Delivery(Builder builder) {
        this.rider = builder.rider;
        this.ask = builder.ask;
        this.stateDelivery = builder.stateDelivery;
        this.datePick = builder.datePick;
    }

    public static class Builder implements CommonModelBuilder<Delivery> {
        private final Rider rider;
        private final Ask ask;
        private final StateDelivery stateDelivery;
        private final LocalDate datePick;
        private LocalDate dateGo;
        private LocalDate dateDone;
        private LocalDate dateCancel;

        public Builder(Rider rider, Ask ask) {
            this.rider = rider;
            this.ask = ask;
            this.stateDelivery = StateDelivery.R_PICK;
            this.datePick = LocalDate.now();
        }

        @Override
        public Delivery build() {
            return new Delivery(this);
        }
    }
}
