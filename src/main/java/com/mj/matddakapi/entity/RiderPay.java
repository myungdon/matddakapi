package com.mj.matddakapi.entity;

import com.mj.matddakapi.interfaces.CommonModelBuilder;
import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class RiderPay {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER) // lazy 확인
    @JoinColumn(name = "riderId", nullable = false)
    private Rider rider;

    @Column(nullable = false)
    private Double payNow;

    public void putPayNow(Double payNow) {
        this.payNow = payNow;
    }

    public RiderPay(Builder builder) {
        this.rider = builder.rider;
        this.payNow = builder.payNow;
    }

    public static class Builder implements CommonModelBuilder<RiderPay>{
        private final Rider rider;
        private final Double payNow;

        public Builder(Rider rider) {
            this.rider = rider;
            this.payNow = 0D;
        }

        @Override
        public RiderPay build() {
            return new RiderPay(this);
        }
    }

    /**
     * 회원 정보 보기용
     * @param builder
     */

    public RiderPay(BuilderRid builder) {
        this.payNow = builder.payNow;
    }

    public static class BuilderRid implements CommonModelBuilder<RiderPay>{
        private final Double payNow;

        public BuilderRid(RiderPay riderPay) {
            this.payNow = riderPay.payNow;
        }

        @Override
        public RiderPay build() {
            return new RiderPay(this);
        }
    }
}
