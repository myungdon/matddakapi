package com.mj.matddakapi.entity;

import com.mj.matddakapi.interfaces.CommonModelBuilder;
import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Attendance{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER) // Lazy 바꿔서 가능한지 확인
    @JoinColumn(name = "riderId", nullable = false)
    private Rider rider;

    @Column(nullable = false)
    private Boolean isWork;

    @Column(nullable = false)
    private String dateCheckWork;

    // 근태 변경

    public void putAttendance() {
        this.isWork = isWork == false ? true : false;
        this.dateCheckWork = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
    }

    /**
     * 근태 등록용
     * @param builder
     */

    public Attendance(Builder builder){
        this.rider = builder.rider;
        this.isWork = builder.isWork;
        this.dateCheckWork = builder.dateCheckWork;
    }

    public static class Builder implements CommonModelBuilder<Attendance>{
        private final Rider rider;
        private final Boolean isWork;
        private final String dateCheckWork;

        public Builder(Rider rider) {
            this.rider = rider;
            this.isWork = false;
            this.dateCheckWork = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        }

        @Override
        public Attendance build() {
            return new Attendance(this);
        }
    }
}
