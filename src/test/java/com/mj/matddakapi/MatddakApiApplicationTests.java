package com.mj.matddakapi;

import org.apache.logging.log4j.util.PropertySource;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@SpringBootTest
class MatddakApiApplicationTests {

    // 숫자를 내림차순 정렬
    @Test
    void contextLoads() {
        int[] texts = {2010, 2934, 1923};
        Integer[] tmpTexts = Arrays.stream(texts).boxed().toArray(Integer[]::new);
        Arrays.sort(tmpTexts, (a, b) -> b - a);

        System.out.println("----------------------");
        System.out.println(Arrays.toString(tmpTexts));
        System.out.println("----------------------");
    }

    // 이름 10개 스트림으로 김씨들만 가져온 후에 오름차순 정렬
    @Test
    void test2() {
        String[] names = {"홍길동", "박기기", "김상호", "한은영", "김옥자", "송종수", "김건우", "이영수", "이경수", "김성훈"};
        List<String> filterList = Arrays.stream(names).filter(name -> name.startsWith("김")).sorted().collect(Collectors.toList());

        System.out.println("----------------------");
        System.out.println(filterList);
        System.out.println("----------------------");
    }

    @Test
    void test3() {
    }
}
